<?php

/**
 * insertcsv.php - Quick and Dirty CSV parser for
 * the insertion of  projector control configs
 * into the database.
 * A. Sebzda 2009(Creator) , M.Daley 2014 (Updated)
 *
 * CSV Style:
 * Room, projector type, port (COM1 or none), hostname, input #, input name [, input #, input name]...
 */


// Parse and verify arguments
if ($argc != 2) die ("Script usage: qnd.php [file.csv]\n");
if ($argv[1] == '--help') die(<<<_HELP_
Script usage: qnd.php [file.csv]

CSV Style: Room, projector type, port(COM1 or none) , hostname, input #, input name [, input #, input name]...
_HELP_
);
if (!is_file($argv[1])) die ($argv[1] . " is not a valid file.\n");


// Open connection to the database
$dsn = 'mysql:dbname=configweb-test;host=127.0.0.1';
$user = 'root';
$passwd = '';
try {
	$dbh = new PDO($dsn, $user, $passwd);
} catch (PDOException $e) {
	die ("Database connect failure: " .  $e->getMessage() . "\n");
}


// Prepare the PDO queries beforehand
$sth = array();
$sthArgs = array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY);
// Servers
$sql = 'INSERT INTO servers (room_number, port_number, type, max_clients)
	VALUES (:room_number, :port_number, :type, 2)';
$sth['servers'] = $dbh->prepare($sql, $sthArgs);
// Projectors
$sql = 'INSERT INTO projectors (room_number, type, port_type, port, ceiling, rear, sound, mute_screen)
	VALUES (:room_number, :type, 0, :port, 1, 0, 1, \'0\')';
$sth['projectors'] = $dbh->prepare($sql, $sthArgs);
// Inputs
$sql = 'INSERT INTO inputs (room_number, input_id, name, number, default_volume)
	VALUES (:room_number, :input_id, :name, :number, 3)';
$sth['inputs'] = $dbh->prepare($sql, $sthArgs);
// Computers
$sql = 'INSERT INTO computers (hostname, hardware_address, service_tag)
	VALUES (:hostname, NULL, NULL)';
$sth['computers'] = $dbh->prepare($sql, $sthArgs);
// Configurations
$sql = 'INSERT INTO configurations (room_number, hostname, active)
	VALUES (:room_number, :hostname, 1)';
$sth['configurations'] = $dbh->prepare($sql, $sthArgs);



// Parse the input file and insert into the database
$row = 0;
$fh = fopen($argv[1], 'r');
while (($data = fgetcsv($fh)) !== false)
{
	// Increment row
	$row++;

	// Extract values, number in squares in the comma value.  Port is for the projector.  COM1 is typical.  "none" denote clone interface
	$room_number = $data[0];
	$type = $data[1];
	$port = $data[2];
	$hostname = $data[3];

	// Basic inserts
	try {
		$dbh->beginTransaction();
		$sth['servers']->execute(array(
			':room_number'	=> $room_number,
			':port_number'	=> '4443',
			':type'		=> '0'
		));
		$sth['servers']->execute(array(
			':room_number'	=> $room_number,
			':port_number'	=> '4444',
			':type'		=> '1'
		));
		$sth['projectors']->execute(array(
			':room_number'	=> $room_number,
			':type'		=> $type,
			':port'		=> $port
		));
		
		$sth['computers']->execute(array(
			':hostname'	=> $hostname
		));
		$sth['configurations']->execute(array(
			':room_number'	=> $room_number,
			':hostname'	=> $hostname
		));
	} catch (PDOException $e) {
		$dbh->rollBack();
		print "Error inserting row " . $row . ": " . $e->getMessage() . "\n";
	}

	
	// Parse the individual inputs
	$inputID = 1;
	$inputIndex = 4; //this is the coloumn index to start parsing from?
	while (isset($data[$inputIndex]) && !empty($data[$inputIndex]))
	{
		// Error checking and variable extraction
		if (!isset($data[$inputIndex + 1]) || empty($data[$inputIndex + 1]))
		{
			print "Row " . $row . " has no corresponding input name for input number " . $inputID;
			$dbh->rollBack();
			break;
		}
		$inputNumber = $data[$inputIndex];
		$inputName = $data[$inputIndex + 1];

		// Database Insert
		try {
			$sth['inputs']->execute(array(
				':room_number'	=> $room_number,
				':input_id'	=> $inputID - 1,
				':name'		=> $inputName,
				':number'	=> $inputNumber
			));
		} catch (PDOException $e) {
			$dbh->rollBack();
			print "Error inserting row " . $row . ": " . $e->getMessage() . "\n";
		}

		// Increment input
		$inputID++;
		$inputIndex += 2;
	}

	// Commit!
	try {
		$dbh->commit();
	} catch (PDOException $e) {
		$dbh->rollBack();
		print "Error inserting row " . $row . ": " . $e->getMessage() . "\n";
	}
}

print "Successfully inserted " . $row . " rows.\n\n";

?>
