<?php

/*
 * Pconfig.xml.php
 * This script takes a single argument for a computer name and returns
 * a formatted XML configuration for the projector control.
 * If no active configuration is found to be stored in the database the
 * script returns nothing.
 *  Place in the webroot of the server. /var/www/files
 */

//Script requires Zend Framework 1.0.3+ to be installed and in the include_path directive
//Require Zend_DB database abstractrion class
require_once 'Zend/Db.php';


//Setup and connect the database
$db = Zend_Db::factory('Mysqli', array(
    'host'     => 'localhost',
    'username' => 'configWeb',
    'password' => 'delicious',
    'dbname'   => 'configweb'
));

//Retrive GET information
if (!isset($_GET['computername'])) { die(); }
$computerName = $_GET['computername'];

//Set up empty array to store config data in
$config = array();

//---------------------------------------------
//SQL Queries
$config['room'] = $db->fetchOne('SELECT room_number FROM configurations WHERE hostname = ? AND active = 1 LIMIT 1', $computerName);
if (empty($config['room'])) { die(); }		//This causes the script to die if no active servers were found.
$config['servers'] = $db->fetchAll('SELECT type, port_number, max_clients FROM servers WHERE room_number = ?', $config['room']);
if (empty($config['servers'])) { die(); }	//No servers set up? Die!
$config['projectors'] = $db->fetchAll('SELECT * FROM projectors WHERE room_number = ?', $config['room']);
$config['inputs'] = $db->fetchAll('SELECT * FROM inputs WHERE room_number = ?', $config['room']);

//---------------------------------------------
//Begin building XML output
$dom = new DOMDocument('1.0');

//Root element
$servConfig = $dom->appendChild($dom->createElement('Projector_Server_Configuration'));

//Server elements
foreach ($config['servers'] as $cnf)
{
	$server = $servConfig->appendChild($dom->createElement('Server'));
	$serverType = $server->appendChild($dom->createElement('Server_Type'));
	$serverType->appendChild($dom->createTextNode($cnf['type']));
	$serverPort = $server->appendChild($dom->createElement('Server_Port'));
	$serverPort->appendChild($dom->createTextNode($cnf['port_number']));
	$maxClients = $server->appendChild($dom->createElement('Max_Clients'));
	$maxClients->appendChild($dom->createTextNode($cnf['max_clients']));
	//Temporary static user/pass sections for server_type 1
	if (1 == $cnf['type'])
	{
		$user = $server->appendChild($dom->createElement('User'));
		$user->appendChild($dom->createTextNode('foo'));
		$password = $server->appendChild($dom->createElement('Password'));
		$password->appendChild($dom->createTextNode(sha1('bar')));
	}
}

//Projector elements
foreach ($config['projectors'] as $prj)
{
	$projector = $servConfig->appendChild($dom->createElement('Projector'));
	$projectorType = $projector->appendChild($dom->createElement('Projector_Type'));
	$projectorType->appendChild($dom->createTextNode($prj['type']));
	$classroom = $projector->appendChild($dom->createElement('Classroom'));
	$classroom->appendChild($dom->createTextNode($prj['room_number']));
	$port = $projector->appendChild($dom->createElement('Port'));
	$port->appendChild($dom->createTextNode($prj['port']));
	$port->setAttribute('connectionType', $prj['port_type']);
	$muteScreen = $projector->appendChild($dom->createElement('Mute_Screen'));
	$muteScreen->appendChild($dom->createTextNode($prj['mute_screen']));
	//Booleans
	if (1 == $prj['ceiling'])
	{
		$ceiling = $projector->appendChild($dom->createElement('Ceiling_Projection'));
	}
	if (1 == $prj['rear'])
	{
		$rear = $projector->appendChild($dom->createElement('Rear_Projection'));
	}
	if (1 == $prj['sound'])
	{
		$sound = $projector->appendChild($dom->createElement('Sound'));
	}

	//Input sections handler for projector control v2 build
	foreach ($config['inputs'] as $inp)
	{
		$input = $projector->appendChild($dom->createElement('Input'));
		$inputName = $input->appendChild($dom->createElement('Input_Name'));
		$inputName->appendChild($dom->createTextNode($inp['name']));
		$inputNumber = $input->appendChild($dom->createElement('Input_Number'));
		$inputNumber->appendChild($dom->createTextNode($inp['number']));
		$inputVolume = $input->appendChild($dom->createElement('Input_Default_Volume'));
		$inputVolume->appendChild($dom->createTextNode($inp['default_volume']));
	}
}


//Format this XML nicely, please
$dom->formatOutput = true;
$output = $dom->saveXML();

//Output the XML
header('Content-Type: text/xml');
print $output;

?>
