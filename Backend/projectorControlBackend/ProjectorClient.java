package projectorControlBackend;
// ProjectorClient Class
// TODO: disconnect inactive clients
import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.io.*;

public class ProjectorClient implements Runnable
{
	private Thread t;
	private Socket clientSocket = null;
	public int socketType;
	/*
	 * socket types:
	 * 0: local socket
	 * 1: public socket
	 */
	private int clientNumber;
	private List<ProjectorClientEventListener> _listeners = 
		new ArrayList<ProjectorClientEventListener>();
	private PrintWriter out;
	private BufferedReader in;
	private boolean connect = false;
	private SocketListener serverSocket;
	
	public ProjectorClient() // creates an empty placeholder for the next client
	{
		
	}
	public ProjectorClient( Socket _clientSocket, int _socketType, 
			int _clientNumber, SocketListener _serverSocket ) // creates a conventional client object
	{
		serverSocket = _serverSocket;
		clientSocket = _clientSocket;
		socketType = _socketType;
		clientNumber = _clientNumber;
		connect = true;
		t = new Thread( this );
		t.start();
	}
	public void setClientID( int _clientNumber )
	{
		clientNumber = _clientNumber;
		t.setName( "Client Thread#" + Integer.toString( clientNumber ) );
	}
	public int getClientID()
	{
		return clientNumber;
	}
	public void disconnectClient( int reasonCode )
	{
		/* reason codes
		 * 0: invalid credentials
		 * 1: no more available connections
		 * 2: shutting down server
		 * 3: server error
		 * 4: client has disconnected
		 */
		connect = false;
		if( clientSocket.isConnected() )
		{
			try
			{
				out.println( "Disconnect - reason code: " + Integer.toString( reasonCode ) );
				clientSocket.close();
			}
			catch( IOException e )
			{
				System.out.println( e.toString() );
			}
		}
		if( socketType == 0 )
		{  // if a local client logs off, shut off projector
			fireProjectorClientEvent( 52 );
		}
		fireProjectorClientEvent( 1 );
			
	}
	private String getClientResponse( String attribute, boolean newLine )
	{
		synchronized( this )
		{
			String response = "";
			out.println( attribute + ": " );
			try
			{
				while( connect &&
						( ( response = in.readLine().trim() ) != null) )
				{
					return response.toLowerCase();
				}
			}
			catch( IOException e )
			{
				// the socket has most likely been closed
				disconnectClient( 4 );
			}
			catch( NullPointerException e)
			{
				// the socket has most likely been closed
				disconnectClient( 4 );
			}
			return "";
		}
	}
	private boolean authenticated()
	{
		if( socketType == 0 && clientSocket.getInetAddress().isLoopbackAddress() ) // local clients authenticate automatically
		{
			out.println( "You were automatically authenticated" );
			return true;
		}
		else if( socketType == 1 ) // network clients require stronger authentication
		{
			String login, password;
			login = getClientResponse( "login", true ).toLowerCase();
			try
			{
				password = SHA1.getSHA1Hash( getClientResponse( "password", true ) );
				return serverSocket.validateUser( login, password );
			}
			catch( SHA1.HashException e )
			{
				System.out.println( e.getError() );
				return false;
			}
		}
		else
		{
			out.println( "Access Denied" );
			return false;
		}
	}
	public void startCommunication()
	{
		connect = true;
	}
	public void run()
	{
		// set up communications
		try
		{
			out = new PrintWriter( clientSocket.getOutputStream(), true );
			in = new BufferedReader( new InputStreamReader( clientSocket.getInputStream() ) );
		}
		catch( IOException e )
		{
			System.out.println( e.toString() );
		}
		// authenticate
		if( authenticated() )
		{
			fireProjectorClientEvent( 0 );
			// wait up to 3 sec for ack from server, check every eighth second
			for( int i = 0; i < 16 && !connect ; i++)
			{
				try
				{
					Thread.sleep(125);
				}
				catch( InterruptedException e ){}
			}
			if( !connect )
			{
//				 no ack from server after 2 seconds
				disconnectClient( 3 );
			}
			else
			{
				// successfull authentication and ack from server
				talkToClient();
			}
		}
		else
		{
			// client has not authenticated
			disconnectClient( 0 );
		}
		// client has disconnected
		disconnectClient( 4 );
	}
	private void talkToClient()
	{
		out.print( "Client Number " + clientNumber  + "\r");
		out.print( "Your IP: " + clientSocket.getInetAddress().getHostAddress() + "\r" );
		out.flush();
		while( connect )
		{
			String response = getClientResponse( "PROJECTOR>" , false );
			if( response.equals( "exit" ) )
			{
				connect = false;
			}
			else
			{
				processCommand( response );
			}
		}
	}
	private void processCommand( String command )
	{
		// execute commands here
		if( command.equals( "status" ) )
		{
			fireProjectorClientEvent( 30 ); 
		}
		else if( command.startsWith( "volume " ) )
		{
			int cmdVol = 70 + Integer.valueOf( command.substring(7) );
			if( cmdVol < 70 )
			{
				cmdVol = 70;
			}
			else if( cmdVol > 90 )
			{
				cmdVol = 90;
			}
			fireProjectorClientEvent( cmdVol );
		}
		else if( command.startsWith( "input " ) )
		{
			int cmdInput = 60 + Integer.valueOf( command.substring(6) );
			if( cmdInput < 60 )
			{
				cmdInput = 60;
			}
			else if( cmdInput > 69 )
			{
				cmdInput = 69;
			}
			fireProjectorClientEvent( cmdInput );
		}
		
		else if( command.equals( "poweron" ) )
		{
			fireProjectorClientEvent( 51 );
		}
		else if( command.equals( "poweroff" ) )
		{
			fireProjectorClientEvent( 52 );
		}
		else if( command.equals( "muteon" ) )
		{
			fireProjectorClientEvent( 53 );
		}
		else if( command.equals( "muteoff" ) )
		{
			fireProjectorClientEvent( 54 );
		}
		else if( command.equals( "info" ) )
		{
			fireProjectorClientEvent( 31 );
		}
	}
	public void receiveServerData( String data )
	{
		synchronized( this )
		{
			out.println( data );
		}
	}
    public synchronized void addProjectorClientEventListener( ProjectorClientEventListener l )
    {
        _listeners.add( l );
    }

    public synchronized void removeProjectorClientEventListener( ProjectorClientEventListener l )
    {
        _listeners.remove( l );
    }

	private synchronized void fireProjectorClientEvent( int type )
	{
		/*
		 * client event types:
		 * 0: successful authentication
		 * 1: client has closed the connection
		 * 30: getState - return XML data about projector
		 * 31: getInfo - return XML data about configuration
		 * 51: setPowerOn
		 * 52: setPowerOff
		 * 70-90: volume
		 */
		ProjectorClientEvent pEvent = new ProjectorClientEvent( this, type );
		Iterator listeners = _listeners.iterator();
		while( listeners.hasNext() )
		{
			( (ProjectorClientEventListener) listeners.next() ).projectorClientEventReceived( pEvent );
		}
	}
} // end of ProjectorClient Class