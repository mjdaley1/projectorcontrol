package projectorControlBackend;
// TODO: Handle missing XML tags
import java.io.File;
import java.io.FileNotFoundException;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException; 
/*
 * 
 * Creates the configuration file for projector server.  Uses XML file created from php
 * script that pulls the data from SQL database.
 */
public class Configuration
{
	private ProjectorConfiguration projectorConfiguration[] = null;
	private ProjectorServerConfiguration projectorServerConfiguration[] = null;
	private boolean loaded = false;
	public Configuration( String file ) throws FileNotFoundException
	{
		try
		{
	        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
	        Document doc = docBuilder.parse( new File( file ) );
            doc.getDocumentElement().normalize();
            if( doc.getDocumentElement().getNodeName().equals( "Projector_Server_Configuration" ) )
            {
	            NodeList listOfServers = doc.getElementsByTagName( "Server" );
	            int serverCount = listOfServers.getLength();
	            projectorServerConfiguration = new ProjectorServerConfiguration[serverCount];
	            for( int s=0; s<serverCount; s++ )
	            {
	                Node serverNode = listOfServers.item( s );
	                if( serverNode.getNodeType() == Node.ELEMENT_NODE )
	                {
	                	projectorServerConfiguration[s] = 
	                		new ProjectorServerConfiguration( s, (Element) serverNode );
	                }//end of if clause
	            }//end of for loop with s var
	            
	            NodeList listOfProjectors = doc.getElementsByTagName( "Projector" );
	            int projectorCount = listOfProjectors.getLength();
	            projectorConfiguration = new ProjectorConfiguration[projectorCount];
	            for( int p=0; p<projectorCount; p++ )
	            {
	                Node projectorNode = listOfProjectors.item( p );
	                if( projectorNode.getNodeType() == Node.ELEMENT_NODE )
	                {
	                	projectorConfiguration[p] = 
	                		new ProjectorConfiguration( p, (Element) projectorNode );
	                }//end of if clause
	            }//end of for loop with p var
	            loaded = true;
            }//end of if clause
        }
		catch ( java.io.FileNotFoundException e )
		{
			System.out.println( "File not found: " + file );
			/*projectorServerConfiguration = new ProjectorServerConfiguration[0];
			projectorConfiguration = new ProjectorConfiguration[0];
			loaded = true;*/
			throw e;
		}
		catch (SAXParseException err)
		{
	        System.out.println ("** Parsing error" + ", line " 
	             + err.getLineNumber () + ", uri " + err.getSystemId ());
	        System.out.println(" " + err.getMessage ());
        }
		catch (SAXException e)
		{
	        Exception x = e.getException ();
	        ((x == null) ? e : x).printStackTrace ();
        }
		catch (Throwable t)
		{
			t.printStackTrace ();
        }
	}
	public ProjectorConfiguration getProjectorConfiguration( int projectorID )
	{
		waitForLoad();
		return projectorConfiguration[projectorID];
	}
	public ProjectorServerConfiguration getProjectorServerConfiguration( int projectorServerID )
	{
		waitForLoad();
		return projectorServerConfiguration[projectorServerID];
	}
	public int getProjectorCount()
	{
		waitForLoad();
		return projectorConfiguration.length;
	}
	public int getServerCount()
	{
		waitForLoad();
		return projectorServerConfiguration.length;
	}
	private void waitForLoad()
	{
		while (! loaded )
		{
			try
			{
				Thread.sleep( 100 );
			}
			catch( InterruptedException e ) {}
		}
	}
}
class ProjectorConfiguration
{
	private int projectorID;
	private int projectorType = 0;
	/*
	0 - 820/800   //This is not used 
	1 - 74c
	2 - 821			//Default type.  This is the BASIC ASCII protocol.
	3 - 830/835  TODO: implement this type
	*/
	private String classroom;
	private String port;
	private int connectionType;
	private boolean ceilingProjection;
	private boolean rearProjection;
	private boolean sound;
	private String mute_screen;
	private ProjectorInput projectorInput[] = null;
	private Element xmlData;
	public final String[] projectorName = { "Epson Powerlite 800/820", "Epson Powerlite 74c",
			"Epson Powerlite 821", "Epson Powerlite 830/835" };
	public final String[] connectionName = { "serial", "tcp/ip" }; //declare the connection method
	
	public ProjectorConfiguration( int _projectorID, Element _xmlData )
	{
		xmlData = _xmlData;
		projectorID = _projectorID;
		projectorType = Integer.valueOf( xmlData.getElementsByTagName( 
				"Projector_Type" ).item(0).getTextContent() );
		classroom = xmlData.getElementsByTagName( 
			"Classroom" ).item(0).getTextContent();
		port = xmlData.getElementsByTagName( 
			"Port" ).item(0).getTextContent();
		connectionType = Integer.valueOf( xmlData.getElementsByTagName( 
			"Port" ).item(0).getAttributes().getNamedItem( 
			"connectionType" ).getNodeValue() );
		ceilingProjection = xmlData.getElementsByTagName( 
			"Ceiling_Projection" ).getLength() > 0;
		rearProjection = xmlData.getElementsByTagName( 
			"Rear_Projection" ).getLength() > 0;
		sound = xmlData.getElementsByTagName( 
			"Sound" ).getLength() > 0;
		mute_screen = xmlData.getElementsByTagName( 
		"Mute_Screen" ).item(0).getTextContent();
		NodeList inputList = xmlData.getElementsByTagName( "Input" );
		int numberOfInputs = inputList.getLength();
		projectorInput = new ProjectorInput[numberOfInputs];
		for( int i = 0; i < numberOfInputs; i++ )
		{
			projectorInput[i] = new ProjectorInput(i, (Element)inputList.item(i) );
		}
		
	}
	public int getProjectorID()
	{
		return projectorID;
	}
	public ProjectorInput getProjectorInput( int projectorInputID )
	{
		return projectorInput[projectorInputID];
	}
	public int getInputCount()
	{
		return projectorInput.length;
	}
	public int getProjectorType()
	{
		return projectorType;
	}
	public String getClassroom()
	{
		return classroom;
	}
	public String getPort()
	{
		return port;
	}
	public int getConnectionType()
	{
		return connectionType;
	}
	public boolean ceilingProjection()
	{
		return ceilingProjection;
	}
	public boolean rearProjection()
	{
		return rearProjection;
	}
	public boolean sound()
	{
		return sound;
	}
	public String getMuteScreen()
	{
		return mute_screen;
	}
}
class ProjectorInput
{
	private int inputID;
	private String inputName;
	private String inputNumber;
	private int inputDefaultVolume;
	private boolean hreverse;
	private boolean vreverse;

	public ProjectorInput( int _inputID, Element xmlData )
	{
		inputID = _inputID;
		inputName = xmlData.getElementsByTagName( "Input_Name" ).item(0).getTextContent();
		inputNumber = xmlData.getElementsByTagName( "Input_Number" ).item(0).getTextContent();
		NodeList inputDefaultVolumeList = xmlData.getElementsByTagName( "Input_Default_Volume" );
		if( inputDefaultVolumeList.getLength() > 0 )
		{
			inputDefaultVolume = Integer.valueOf( inputDefaultVolumeList.item(0).getTextContent() );
		}
		else
		{
			inputDefaultVolume = -1;
		}
		hreverse = xmlData.getElementsByTagName( "Hreverse" ).getLength() > 0;
		vreverse = xmlData.getElementsByTagName( "Vreverse" ).getLength() > 0;
		
	}
	public int getInputID()
	{
		return inputID;
	}
	public String getInputName()
	{
		return inputName;
	}
	public String getInputNumber()
	{
		return inputNumber;
	}
	public int getInputDefaultVolume()
	{
		// -1 if no sound on this input
		return inputDefaultVolume;
	}
	public String getHReverse( boolean rearProjection )
	{
		if( hreverse ^ rearProjection )
		{
			return "ON";
		}
		else
		{
			return "OFF";
		}
	}
	public String getVReverse( boolean ceilingProjection )
	{
		if( vreverse ^ ceilingProjection )
		{
			return "ON";
		}
		else
		{
			return "OFF";
		}
	}
}
class ProjectorServerConfiguration
{
	private int projectorServerID;
	private int serverType;
	private int serverPort;
	private int maxClients;
	private String user;
	private String pass;
	public ProjectorServerConfiguration( int _projectorServerID, Element xmlData )
	{
		projectorServerID = _projectorServerID;
		serverType = Integer.parseInt( xmlData.getElementsByTagName( 
				"Server_Type" ).item(0).getTextContent() );
		serverPort = Integer.parseInt( xmlData.getElementsByTagName( 
			"Server_Port" ).item(0).getTextContent() );
		maxClients = Integer.parseInt( xmlData.getElementsByTagName( 
			"Max_Clients" ).item(0).getTextContent() );
		try
		{
			user = xmlData.getElementsByTagName( "User" ).item(0).getTextContent();
			pass = xmlData.getElementsByTagName( "Password" ).item(0).getTextContent();
		}
		catch( Exception e )
		{
			user=null;
			pass=null;
		}
	}
	public int getProjectorServerID()
	{
		return projectorServerID;
	}
	public int getServerType()
	{
		return serverType;
	}
	public int getServerPort()
	{
		return serverPort;
	}
	public int getMaxClients()
	{
		return maxClients;
	}
	public String getUsername()
	{
		return user;
	}
	public String getPassword()
	{
		return pass;
	}
}