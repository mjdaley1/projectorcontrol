package projectorControlBackend;
import java.util.EventObject;

public class ProjectorEvent extends EventObject
{
	private static final long serialVersionUID = 1L;
	private String _type;

    public ProjectorEvent( Object source, String type )
    {
        super( source );
        _type = type;
    }

    public String Type()
    {
		return _type;
	}
    
}
