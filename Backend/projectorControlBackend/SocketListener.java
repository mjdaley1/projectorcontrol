package projectorControlBackend;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.InetAddress;
import java.net.ServerSocket;

public class SocketListener implements Runnable, ProjectorClientEventListener
	{
		private Thread socketThread;
		private ServerSocket serverSocket;
		private int socketPort;
		private int socketType;
		private int maxConnections;
		private boolean running = true;
		private final String[] socketTypeName = { "local", "public" };
		private ProjectorClient[] projectorClient;
		private EpsonProjectorControl pControl;
		private String username;
		private String password;

		public SocketListener( int _socketType, int _socketPort,
				ProjectorClient[] _projectorClient, EpsonProjectorControl _pControl,
				String _username, String _password )
		{
			socketPort = _socketPort;
			socketType = _socketType;
			maxConnections = Array.getLength( _projectorClient );
			projectorClient = _projectorClient;
			pControl = _pControl;
			username = _username;
			password = _password;
			// initialize client objects
			for( int i = 0; i < maxConnections; i++ )
			{
				projectorClient[i] = null;
			}
			socketThread = new Thread( this, socketTypeName[socketType] + " thread" );
			socketThread.start(); // calls run in its own thread
			
		}
		public boolean validateUser( String login, String pass )
		{
			return ( login.equals( username ) && pass.equals( password ) );
		}
		public void projectorClientEventReceived( ProjectorClientEvent pEvent )
		{
			switch( pEvent.type() )
			{
			case 0:  // client has connected
				int nextClient = nextAvailableClientNumber();
				if( nextClient < maxConnections )
				{
					// assign namespace to successfully authenticated client
					projectorClient[nextClient] = pEvent.projectorClient();
					// let the client object know who it is
					projectorClient[nextClient].setClientID( nextClient ); 
					projectorClient[nextClient].startCommunication();
				}
				else // disconnect
				{
					pEvent.projectorClient().disconnectClient( 1 );
				}
				break;
				
			case 1:  // client has disconnected
				int clientNum = pEvent.projectorClient().getClientID();
				if( clientNum >= 0 && clientNum < Array.getLength( projectorClient ) )
				{
					projectorClient[clientNum] = null;
				}
				break;
			case 30:    //This is received from projectorClient
				// return projector info to client
				pEvent.projectorClient().receiveServerData( generateXMLStatus() );
				break;
			case 31:
				// return configuration info to client
				pEvent.projectorClient().receiveServerData( generateXMLConfigInfo() );
				break;
			case 51:
				// pControl.turnProjectorOn();
				new PowerThread( 1 ).runThread();    //custom made class that will continue to run.
				break;
			case 52:
				// Control.turnProjectorOff();
				new PowerThread( 0 ).runThread();   //custom made class that will continue to run.
				break;
			case 53:
				pControl.setProjectorToStandBy();
				break;
			case 54:
				pControl.returnProjectorFromStandBy();
				break;
			case 60:
			case 61:
			case 62:
			case 63:
			case 64:
			case 65:
			case 66:
			case 67:
			case 68:
			case 69:
				// inputs
				pControl.setProjectorInput( pEvent.type() - 60 );
				break;
			case 70:
			case 71:
			case 72:
			case 73:
			case 74:
			case 75:
			case 76:
			case 77:
			case 78:
			case 79:
			case 80:
			case 81:
			case 82:
			case 83:
			case 84:
			case 85:
			case 86:
			case 87:
			case 88:
			case 89:
			case 90:
				pControl.setVolume( pEvent.type()-70 );
				break;			
			default:
				System.out.println( "Unknown case for projector event" );
			}
		}
		class PowerThread {
			  private Thread t;
			  private String name;
			  private int value;
			  public PowerThread( int value )  // 1 for on, 0 for off
			  {
				  this.value = value;
				  if( value < 1 )
				  {
					  this.name = "PowerOffThread";
				  }
				  else
				  {
					  this.name = "PowerOnThread";
				  }
			  }
			  public void runThread() 
			  {
			    if ( t == null ) 
			    {
			      t = new Thread( name ) 
			      {
			        public void run() 
			        {
			        	if( value < 1 )
			        	{
			        		pControl.turnProjectorOff();
			        	}
			        	else
			        	{
			        		pControl.turnProjectorOn();
			        	}
			        }
			      };
			      t.start();
			    }
			  }
			}
		private String generateXMLStatus()
		/*
		 * pControl refers to the EpsonProjectorControl.class
		 */
		{
			String xmlStatus = "<?xml version=\"1.0\"?><Status>";
			xmlStatus += "<State>" + pControl.getState() + "</State>";
			String[][] gotSettings = pControl.getSettings();
			for( int s = 0; s < gotSettings[0].length; s++ )
			{
				xmlStatus += "<" + gotSettings[0][s] + " age=\"" + gotSettings[2][s] + "\">" + 
					gotSettings[1][s] + "</" + gotSettings[0][s] + ">";
			}
			xmlStatus+= "<Input>" + pControl.determineCurrentInput() + "</Input>";
			xmlStatus+= "<Requested_Input>" + pControl.getDefaultInput() + "</Requested_Input>";
			xmlStatus+= "</Status>";
			return xmlStatus;
		}
		private String generateXMLConfigInfo()
		{
			ProjectorConfiguration projectorConfiguration = pControl.getConfiguration();
			String xmlConfigInfo = "<?xml version=\"1.0\"?><Configuration_Info>";
			xmlConfigInfo += "<Classroom>" + projectorConfiguration.getClassroom() + "</Classroom>";
			xmlConfigInfo += "<Projector_Type id=\"" + projectorConfiguration.getProjectorType() +
				"\">" + projectorConfiguration.projectorName[projectorConfiguration.getProjectorType()] + 
				"</Projector_Type>";
			xmlConfigInfo += "<Connection_Type id=\"" + projectorConfiguration.getConnectionType() + 
				"\">" + 
				projectorConfiguration.connectionName[projectorConfiguration.getConnectionType()] + 
				"</Connection_Type>";
			xmlConfigInfo += "<Port>" + projectorConfiguration.getPort() + "</Port>";
			xmlConfigInfo += "<Mute_Screen>" + projectorConfiguration.getMuteScreen() + "</Mute_Screen>";
			xmlConfigInfo += "<Inputs>";
			for( int i = 0; i < projectorConfiguration.getInputCount(); i++ )
			{
				ProjectorInput projectorInput = projectorConfiguration.getProjectorInput( i );
				xmlConfigInfo += "<Input id=\"" + projectorInput.getInputID() + "\">" + 
					projectorInput.getInputName() + "</Input>";
			}
			xmlConfigInfo += "</Inputs>";
			xmlConfigInfo += "</Configuration_Info>";
			return xmlConfigInfo;
		}
		private synchronized int nextAvailableClientNumber()
		{
			for( int i = 0; i < maxConnections; i++ )
			{
				if( projectorClient[i] == null )
				{
					projectorClient[i] = new ProjectorClient();
					return i;
				}
			}
			return maxConnections;
		}
		public void run()
		{
			// open a port to listen on
			try
			{
				if( socketType == 0 )
				{
					byte[] address = {127,0,0,1};
					serverSocket = new ServerSocket( socketPort, maxConnections, 
							InetAddress.getByAddress( address ) );
				}
				else if( socketType == 1 )
				{
					serverSocket = new ServerSocket( socketPort, maxConnections );
				}
			}
			catch (IOException e)
			{
			    System.out.println( e.toString() );
			}
			while( running )
			{
				// listen for clients
				try
				{
					// java blocks at .accept() until a client connects
					ProjectorClient tempClient = 
						new ProjectorClient( serverSocket.accept(), socketType, maxConnections, this );
					tempClient.addProjectorClientEventListener( this );
				}
				catch (IOException e) 
				{
				    System.out.println( e.toString() );
				}
			}
			// close the connection
			try
			{
				serverSocket.close();
			}
			catch( IOException e )
			{
				System.out.println( e.toString() );
			}
		}
	}
