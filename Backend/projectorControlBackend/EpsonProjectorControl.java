package projectorControlBackend;
// EpsonProjectorControl Class
// Adam Paul
// CCRI Classroom Support
// 2006
//Mike Daley, 2013
import gnu.io.*;

import java.util.*;
import java.io.*;
/*
 * This class talks to the projector.  Currently talks directly over the COM port.
 * 
 * 
 * 
 */

public class EpsonProjectorControl implements Runnable, SerialPortEventListener  //TODO: Add networkEventListener
{
	Enumeration<CommPortIdentifier> portList;
	CommPortIdentifier portId;
	SerialPort serialPort;
	OutputStream outputStream;
	InputStream inputStream;
	Thread communicationThread = new Thread( this );

    private List<ProjectorEventListener> _listeners = new ArrayList<ProjectorEventListener>();
    private CommandQueue commandQueue = new CommandQueue( 50 ); 
    
	// status variables
    
    private int state = 4;
    private String source = ""; //hold the requested source in a variable. Will allow for latching I think...  Should always default to Classroom PC!
    private String commandSent = ""; // this is going to hold the command Sent variable.
    private int	sourceID = 0;   //starts up with the default source as 0.  Typically classroom PC.
    private int powerSent = 0; //variable to hold the counter for PWR being sent.
    private String settings[] = { "SOURCE","MSEL","LAMP","MUTE","HREVERSE","VREVERSE","VOL"};
    private String currentSettings[] = { "","","","","","",""}; // store for info read back from proj
    private long currentSettingsAge[] = { -1,-1,-1,-1,-1,-1,-1,};  // time in millis last update occurred
	private boolean projectorResponded = false;  // buffer trigger
	private boolean continueCommunication = false;  // loop control
	public boolean noProjectorComm = false; //this is used for there being no projector connected over rs232 or tcpip.

	// configuration
	private ProjectorConfiguration projectorConfiguration;

	public EpsonProjectorControl( ProjectorConfiguration _projectorConfiguration )
	{
		projectorConfiguration = _projectorConfiguration;
		
		
		//Set a default input from the configuration.  Use the first input in the list.
		ProjectorInput defaultInput = projectorConfiguration.getProjectorInput(0);  //use input0 as default source for the projector.
		source = defaultInput.getInputNumber();
		System.out.println("Default Source is : " + defaultInput.getInputNumber() );
		
		
		if(projectorConfiguration.getPort().contains("none"))
		{
			//skip setting up the connection as there is none.
			System.out.println("No Projector comm available.  Dummy setup for cloning screen.");
			noProjectorComm=true; //tell the program there is no communication available.
			continueCommunication = true;
		}
		else
		{
			setUpConnection();
		}
		
	}
	public void communicationStart()
	{
		continueCommunication = true;
		communicationThread.start();  //this calls the method run()
	}
	public void communicationStop()  //this is not called currently.
	{
		continueCommunication = false;
	}
	
    @SuppressWarnings("unchecked")
	private void setUpConnection()
    {
        System.out.println("running setUpConnection");
    	portList = (Enumeration<CommPortIdentifier>) CommPortIdentifier.getPortIdentifiers();

        while (portList.hasMoreElements() && noProjectorComm == false)
        {
            portId = portList.nextElement();

            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL)
            {
                if (portId.getName().equals( projectorConfiguration.getPort() ))
                {
                    try
                    {
                        serialPort = (SerialPort) portId.open( "Projector", 2000 );
                    } catch (PortInUseException e) {}
                    try
                    {
                        outputStream = serialPort.getOutputStream();
                    } catch (IOException e) {}
                    try
                    {
						inputStream = serialPort.getInputStream();
					} catch (IOException e) {}
					try
					{
					    serialPort.addEventListener( this );
					} catch (TooManyListenersException e) {}
        			serialPort.notifyOnDataAvailable(true);
                    try
                    {
                        serialPort.setSerialPortParams(9600,
                            SerialPort.DATABITS_8,
                            SerialPort.STOPBITS_1,
                            SerialPort.PARITY_NONE);
                    } catch (UnsupportedCommOperationException e) {}
                }
            }
        }
        blindPowerStateCheck(); //check for initial state at startup.  Only runs on COM ports.	
    }
	public void run()
	{
		int noResponse = 0; //Variable to hold the no response count.  This is a count per 100 ms.
		
		while( continueCommunication )
		{
			
			if(noProjectorComm == true)
			{
				setDummyState();
				try {
					Thread.sleep(60000); //sixty second nap..
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else   // command executing statement
			{
					
					if(projectorResponded && !commandQueue.queueEmpty())  //waits for a response to the previous command before sending another.
					{
						commandSent = commandQueue.executeNextCommandFromQueue();
						sendProjectorCommand( commandSent ); 
						noResponse=0; 
					}
					
					
					else   //when <821 projector is off it will not send a : to respond to commands.
					{
						if(noResponse >= 15)   //will run after 10 seconds of no responses.  This is dependant on the Thread.sleep below.  (100*100=10000 ms)
						{
							// System.out.println("No Response from projector for 15 senconds.  Clearning Queue, Running blindPowerStateCheck. "
							//		+ "\n Could be powering On or Off, or Frontend is not open.");
							blindPowerStateCheck();//Check and make sure the projector is off. Use blind, so the command actually will fire!  
							noResponse=0; //reset no response to 0
							if(!projectorResponded) //Lets see if the projector will respond to something.
							{
								System.out.println("Assume projector is off.  Switch the interface to the off state. State 4");
								changeState(4);  //Accept that the projector is off, and move on.  Fixes < 821p Models.  These do not respond when projector is off. Projector could also be missing.  This will be tested during power on.
								commandQueue.clearCommandQueue(); //only clear if the projector is not responding
							}
							
						}//end if
						else
						{
							noResponse++;
							//System.out.println("No response for " + noResponse + " seconds.  " + commandQueue.executeNextCommandFromQueue() +"is queued.");
						}//end else
					
					}//else
					
				}//else
			
			
			
			try
			{
				Thread.sleep( 1000 );    //The delay before running the loop again.  Each loop a command will either send, or 
			}
			catch( InterruptedException e ) {}
			}
		
			
		
	}//end run method
	
	/*
	The state variable gets set elsewhere in the program.  When changeState is called, you are able to specify the 
	the wanted functionality (power on / power off), and then the state variable is read to further the cause.
	*/
	private void changeState( int _state )  //Take the variable that is passed and assign it to _state
	{
		System.out.println("Requested State" + _state +" ,Currently State" + state);  // A little debug info.  //Current state is what is sent to the frontend!!
		
		switch( _state ) // _state is the currently requested state that we should bring the projector to. (Sent to Frontend?)
		{
		case 0:
			// projector is accepted to be off ( called internally only )   again, sent to the frontend only.
			state = _state;
			sourceID = 0;   //reset the default source to Classroom PC.
			break;
		case 1: //turnProjectorOn();  
			switch( state )  // state is the previously recorded or requested of the projector This variable can be set anywhere!, // nested switch control block  
				{
					case 1: 
						System.out.println("Powering ON already!"); //will only send PWR ON until the projector responds! Yeay!
						
						break;
					case 2:
						System.out.println("Projector is ON.  Why turn on again?");
						break;
					case 3:
						System.out.println("Can't turn on, turing off");
						break;
					case 4: // Default state, projector status is unknown.  We will try!
						//This state means the projector does not talk while off, or it is unplugged ( Power or rs232)
					case 5: // current state is error, but we'll try again
					
					case 6: //projector has not responded to previous attempts to turn it on.  This state should prompt the user to call classroom support.  IT will send an email?	
					case 0: // This matches when the projector is currently OFF!
						state = _state; //force the state to stay as 1 (powering on) as long as the projector has not responded with power on.
								if(commandQueue.commandNotInQueue("PWR ON"))
								{
							
									commandQueue.addCommandToQueue("PWR ON");
								}
								projectorResponded = true; //this is needed to allow the PWR ON to be sent.
								
								setProjectorInput(sourceID);   //trigger default / choosen input
								System.out.println("Set default input: "+ sourceID);
						break;
						
				}
		case 2:  // projector is ON, this is called from ParseResponse
			state = _state;
			break;
		
		case 3:  //turnProjectorOff()  reads the current state to determine what to do.
			
			switch( state )  //use the current state to block unnecessary changes.
			{
			case 0:
				System.out.println("Projector is already OFF");
				break;
			case 1:
				 System.out.println("in the process of turning on... sorry");
				break;
			case 3:
				System.out.println("already in the process of turning off");
				/*Not needed!!!
				 * if(commandQueue.commandNotInQueue("PWR OFF"))
				{
					commandQueue.addCommandToQueue("PWR OFF");
				}
				*/
				break;
			case 2:
				System.out.println("Start Turning Projector OFF");  //Projector is currently ON.
				
			case 4: // I don't know if it's on, but we can try.  Default state.
			case 5: // I think something is wrong, but we'll try
				state = _state;  //Set the status that we are powering off.
				if(commandQueue.commandNotInQueue("PWR OFF"))
				{
					commandQueue.addCommandToQueue("PWR OFF");
				}
				projectorResponded = true; //this is needed to allow the PWR ON to be sent.
				commandQueue.addCommandToQueue("PWR?");
				break;  //end case 5
			}
			break; //End switch
		case 4: //Default state  Nothing happening here
			state = _state;
			sourceID = 0;  //reset the default source. 
			break;
		case 5:
			// This is an error state.  We will report this when the projector is logging some sort of error.
			// Should not prevent turning the projector ON/OFF
			// TODO: put an alert here
			state = _state;
			break;
		case 9:
			//this is for a dummy state
			state = _state;   //This tells the program that we are running the dummy mode.
			break;
		default:
			// we should never wind up here
			System.out.println( "The state you've asked for is invalid" );
		}
	}
	private void blindPowerStateCheck()
	{
			sendProjectorCommand( "PWR?" );    //this is needed to kick off the command queue.
			System.out.println("Blind power state check");
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	public void serialEvent(SerialPortEvent event)
	{
	   switch(event.getEventType())
		{
			case SerialPortEvent.BI:
			case SerialPortEvent.OE:
			case SerialPortEvent.FE:
			case SerialPortEvent.PE:
			case SerialPortEvent.CD:
			case SerialPortEvent.CTS:
			case SerialPortEvent.DSR:
			case SerialPortEvent.RI:
			case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
				break;
			case SerialPortEvent.DATA_AVAILABLE:
				byte[] readBuffer = new byte[25];
				try
				{
					try
					{
						Thread.sleep(300); // wait for stream to fill
					}
					catch ( InterruptedException e ) {}
		/*
		 * Read the input from the serial line.  Separate all carriage returns into new lines.   
		 * Catch the exception if the input buffer is unavailable.
		 */
					while ( inputStream.available() > 0 )
					{
						inputStream.read( readBuffer );
					}
					for( int i = 0; i < readBuffer.length; i++ )
					{
						if( readBuffer[i] == 13 )
							readBuffer[i] = 10; // replace all carriage returns with newlines
					}

				}
				catch (IOException e)
				{
					System.out.println( "Failure:  Unable to read input buffer" );
				}
				parseResponse( new String( readBuffer ) );
		}
	}
/*
 * Break responses into two categories.  PWR response and settings responses (including source?)
 * Typical response format is :PWR=01   or :SOURCE=41.
 * The colon is a reply from the projector for the get command of PWR? or SOURCE?  You should always wait for the colon before sending
 * another command.
 * 
 * 
 */
	private void parseResponse ( String responseReceived ) 
	{
		int colonLocation = responseReceived.indexOf( ":" );
		
		if(responseReceived.contains( "ERR"))  //This is returned when the projector is unable to process a command. Ex.  During power on, ERR is returned until the colon for the PWR ON command is sent back. 
		{
			//TODO: We should switch state here to something that declares an error.  This would be helpful in processing the command again / returning some important information.
			System.out.println("ERR returned from the projector. Take a short break. Command will be sent again.");
			
			projectorResponded = true; // set true but do not remove last command!  This will cause it to resend.
			
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}//catch
		}
		/*
		 * The next block sets projectorResponded to true, and removes the command from the queue. The command is only removed
		 * if it matches what was sent previously.  It was possible to send a command, clear the queue, then have this else if remove
		 * the next command since it thinks that it was sent.  
		 */
		
		else if( colonLocation >= 0 )
		{
			projectorResponded = true;
			System.out.println("Projector Responded" + responseReceived);
			//compare last command SENT to the command that will be removed.  They should match.  If not DO NO remove.
			
			if(!commandQueue.queueEmpty())
			{
				if( commandSent == commandQueue.executeNextCommandFromQueue() ) //Only remove the command that was sent.
				{
				 commandQueue.removeCommandFromQueue();
				}
			}
		}
		
		int responseLocation = responseReceived.indexOf( "PWR=" ) + 4;   //zero out the response so we can get the numeric value
		String responseValue = "";   //CLEAR response value?
		
		if( responseLocation > 3 && responseReceived.length() > 4 )
		{
			responseValue =  responseReceived.substring( responseLocation, colonLocation-1 );
			if( responseValue.equals( "1" ) || responseValue.equals( "01" ) )
			{
				changeState(2);   //this will set the state = 2 inside the switch.  Power is ON
			}
			else if( responseValue.equals( "0" ) || responseValue.equals( "00" ) || responseValue.equals( "0" ) || responseValue.equals( "04" ))//server returns a value of 0 (off) or 4 (also off, projector off communication ON in standby) esc/vp21
			{
				changeState(0);	//this will set the state = 0 inside the switch.  Power is OFF
			}
			else if( responseValue.equals( "3" ) || responseValue.equals( "03" ))
			{
				changeState(3);	//We are powering off here. 
			}
		}
		
		else //TODO: check into the setting age in here.
		{
			for( int i = 0; i < settings.length; i++ )
			{
				String searchTerm = settings[i] + "=";
				int searchTermLength = searchTerm.length();
				int responseLoc = responseReceived.indexOf( searchTerm ) + searchTermLength;
				if( responseLoc > searchTermLength-1 && responseReceived.length() > searchTermLength )
				{
					switch( i )
					{
					case 6:
						currentSettings[i] =  String.valueOf( convertFromProjVol( 
								Integer.valueOf( responseReceived.substring( 
								responseLoc, colonLocation-1 ) ) ) );
						break;
					default:
						currentSettings[i] =  responseReceived.substring( responseLoc, colonLocation-1 );
					}
					currentSettingsAge[i] = System.currentTimeMillis();
				}
			}
		}
	}
	private void sendProjectorCommand( String commandString )
	{
		synchronized( this ) // prevents writing to the output stream concurrently.
		{
			
			projectorResponded = false;
			commandString += "\r";
			// System.out.println("Sending Command: " + commandString);
			try
			{
				outputStream.write( commandString.getBytes() );
				System.out.println("Sent: " + commandString);
			}
			catch (IOException e)
			{
				System.out.println( "communication error while sending command: " + commandString );
			}
		}
	}
	
	public void setVolume( int newVolume )
	{
		//input accepted as integer 0 through 20
		commandQueue.addCommandToQueue( "VOL " + Integer.toString( convertToProjVol( newVolume ) ) );
		commandQueue.addCommandToQueue( "VOL?");
	}
	public void setDummyState()
	{
		state = 9; //this is a dummy state.  
		System.out.println("Setting state 9,  dummy state.");
	}
	
	/*
	 * For the next two methods make sure that the queue does not contain the command that will be sent.
	 * 
	 */
	public void turnProjectorOff()
	{
		if(commandQueue.commandNotInQueue("PWR OFF"))
		{
				commandQueue.clearCommandQueue();
		}
		changeState(3);  
	}
	public void turnProjectorOn()
	{
		if(commandQueue.commandNotInQueue("PWR ON"))
				{
				commandQueue.clearCommandQueue();
				}
		changeState(1); 
	}
	public void setProjectorToStandBy()
	{
		commandQueue.clearCommandQueue();
		commandQueue.addCommandToQueue( "MUTE ON" );
		commandQueue.addCommandToQueue( "MUTE?" );
	}
	public void returnProjectorFromStandBy()
	{
		commandQueue.clearCommandQueue();
		
		commandQueue.addCommandToQueue( "MUTE OFF" );
		commandQueue.addCommandToQueue( "MUTE?" );
	}
	public void setProjectorInput( int inputID )
	{
		sourceID = inputID;  // set the sourceID variable to hold the choosen inputID.  This allows checking and making sure the input stays current.   :(  This will cause a problem with the remote!!!  UGH.
		
		//Turn on the projector if it is not already on!!
		if(state == 0 || state == 4)
			{
				turnProjectorOn(); 
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		
		if(commandQueue.commandNotInQueue("PWR ON"))   //Only clear the queue if PWR is not in there...
		{
			System.out.println("setProjectorInput is clearning the queue.  'PWR ON' is NOT in queue.");
			commandQueue.clearCommandQueue(); //set the input right away!!
		}
		else ///this is needed for default input to work.
		{
			System.out.println("Changing source, PWR ON is in queue so Not Clearing Queue....");
		}
		//remove all existing source commands.	
		while(!commandQueue.commandNotInQueue("SOURCE 41"))
		{
			commandQueue.removeIfInQueue("SOURCE 41" );
		}
		while(!commandQueue.commandNotInQueue("SOURCE 21"))
		{
			commandQueue.removeIfInQueue("SOURCE 21");
		}
		while(!commandQueue.commandNotInQueue("SOURCE 22"))
		{
			commandQueue.removeIfInQueue("SOURCE 22" );
		}
		
		ProjectorInput input = projectorConfiguration.getProjectorInput( inputID );
		commandQueue.addCommandToQueue( "SOURCE " + input.getInputNumber() );
		source = input.getInputNumber();  //always the value that will be send with the source command
		if( input.getInputDefaultVolume() > -1 )
		{
			setVolume( input.getInputDefaultVolume() );
		}
		commandQueue.addCommandToQueue( "VREVERSE " + input.getVReverse( projectorConfiguration.ceilingProjection() ) );
		commandQueue.addCommandToQueue( "HREVERSE " + input.getHReverse( projectorConfiguration.rearProjection() ) );
		//disabled checking of these settings.  No action is taken based on the return value.
		
		
		//Disabled all checks since it takes up to 5 seconds to actually get a response.  just run status more often?
		commandQueue.addCommandToQueue("SOURCE?");  //check the new source.  This will update the setting accordingly, and allow for a fast update.  ~8 seconds :(
		// commandQueue.addCommandToQueue( "VREVERSE?" );
		// commandQueue.addCommandToQueue( "HREVERSE?" );
		//commandQueue.addCommandToQueue(");  add something here to handle the projector setting up resolution?  This could handle 16:9 vs. 4:3 or other issues.
		
	}
	
	public int getState() //this is called when the status command is sent from the frontend.
	{
		if(commandQueue.commandNotInQueue( "PWR?" ))
				{
					commandQueue.addCommandToQueue( "PWR?" );    //query the power state of the projector.  This will allow it to respond to manually turning non the projector
				}
		getSettingsFromProjector();  //POLL FOR CURRENT INPUTS/MUTE/SETTINGS
		return state;  //returns 
	}
	
	private void getSettingsFromProjector()  //TODO store the last run time of the currentSettings.  This is not done currently.
	{
		
		for( int i = 0; i < settings.length; i++ )
		{
			String command = settings[i] + "?";
			//System.out.println(currentSettingsAge[i]); // print the age of the current setting.
			if(state==2) //Only check for settings when projector is on.
			{
				/*if( System.currentTimeMillis() - currentSettingsAge[i] > 30000    //refresh settings only every 30 seconds.
						&& commandQueue.commandNotInQueue( command ) )
				{
					commandQueue.addCommandToQueue( command );
				}*/
				if (commandQueue.commandNotInQueue( command) )
				{
					commandQueue.addCommandToQueue( command );
				}
			}
			
		}
	}
	public String[][] getSettings()   //Use the gathered settings data.
	{
		int length = settings.length;
		String[][] _settings = new String[3][length];
		_settings[0] = settings;
		_settings[1] = currentSettings;
		for( int s = 0; s < length; s++ )
		{
			long age = -1; 
			if( !( currentSettingsAge[s] == -1 ) )
			{
				age = System.currentTimeMillis() - currentSettingsAge[s];
			}
			_settings[2][s] = String.valueOf( age );
		}
		return _settings;
	}
	public ProjectorConfiguration getConfiguration()
	{
		return projectorConfiguration;
	}
	public String determineCurrentInput()  //called from SocketListener.
	{
		for( int i = 0; i < projectorConfiguration.getInputCount(); i++ )
		{
			ProjectorInput projectorInput = projectorConfiguration.getProjectorInput( i );
			if( projectorInput.getInputNumber().equals( currentSettings[0]) )
			{
				if( projectorInput.getHReverse( projectorConfiguration.rearProjection() ).equals( currentSettings[4] ) )
				{
					if( projectorInput.getVReverse( projectorConfiguration.ceilingProjection() ).equals( currentSettings[5] ) )
					{
						return String.valueOf( i );
					}
				}
			}
		}
		return "-1";
	}
	public int getDefaultInput()
	{
		return sourceID;
	}
	private int convertFromProjVol( int projVol )
	{
		final int vols[][] = {
				{0,8,24,32,49,57,74,82,98,106,123,139,156,164,180,189,205,213,230,238,255},
				{0,8,24,33,49,57,74,82,99,107,123,132,148,156,173,181,198,206,222,231,247},
				{0,12,24,36,48,60,73,85,97,109,121,134,146,158,170,182,195,207,219,231,243}
				};
		return Arrays.binarySearch( vols[projectorConfiguration.getProjectorType()], projVol );
	}
	private int convertToProjVol( int vol )
	{
		System.out.println ( "Converting from" + vol );
		final int vols[][] = {
			{0,9,25,33,50,58,75,83,99,107,124,140,157,165,181,190,206,214,231,239,255},
			{0,8,24,33,49,57,74,82,99,107,123,132,148,156,173,181,198,206,222,231,247},
			{0,12,24,36,48,60,73,85,97,109,121,134,146,158,170,182,195,207,219,231,243}
			};
		System.out.println (vols[projectorConfiguration.getProjectorType()][vol]);
		return vols[projectorConfiguration.getProjectorType()][vol];
	}
    public synchronized void addProjectorEventListener( ProjectorEventListener l )
    {
        _listeners.add( l );
    }
    public synchronized void removeProjectorEventListener( ProjectorEventListener l )
    {
        _listeners.remove( l );
    }
}

class CommandQueue
{
	// storage for command queue
    private List<String> _commands;
	public CommandQueue( int length )
	{
		_commands = new ArrayList<String>( length );
	}
	//why is this commented out?
	/*private synchronized void fireProjectorEvent( String type )
	{
		ProjectorEvent pEvent = new ProjectorEvent( this, type );
		Iterator listeners = _listeners.iterator();
		while( listeners.hasNext() )
		{
			( (ProjectorEventListener) listeners.next() ).projectorEventReceived( pEvent );
		}
	}*/
	public boolean commandNotInQueue( String command )
	{
		if (_commands.indexOf( command ) < 0 )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public synchronized void removeIfInQueue ( String command)
	{
		if(!_commands.isEmpty())
		{
			if(_commands.remove(command))
			{
				System.out.println(command + ":  Removed from queue");
			}
		}
			
	}
	
    public synchronized void addCommandToQueue( String strCommand )
    {
        _commands.add( strCommand );
        System.out.println( "Command queued: " + strCommand );
        try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    public synchronized void clearCommandQueue()
    {
       System.out.println("Clearing the command queue");
    	_commands.clear();
    }
    public synchronized String executeNextCommandFromQueue() //move executed command to history.  Only keep one command.
    {
    	String command = "";
    	
    	if( !_commands.isEmpty() )
    	{
    		command =  _commands.get(0);
    		//_commands.remove(0);   // Not removing the command yet.  This will occur only after the projector successfully responds to the command.
    	}
       	return command;
    }
    //check if the queue is empty to stop it from executing blank statements.
    public synchronized boolean queueEmpty()
    {
	    if( _commands.isEmpty() )
	    {
	    	return true;
	    }
	    return false;
    }
    
    public synchronized String removeCommandFromQueue()
    {
    	String removed = "";
    	if(!_commands.isEmpty())
    	{
    		removed = _commands.remove(0); //.remove returns the value that is removed.
    		System.out.println("command removed: " + removed);
    	}
    	
    	return removed;
    	
    }
    
}
