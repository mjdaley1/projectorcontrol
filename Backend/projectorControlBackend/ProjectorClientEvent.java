package projectorControlBackend;
import java.util.EventObject;

public class ProjectorClientEvent extends EventObject
{
	private static final long serialVersionUID = 1L;
	private int _type;
	private ProjectorClient _projectorClient;

    public ProjectorClientEvent( ProjectorClient source, int type )
    {
        super( source );
        _type = type;
        _projectorClient = source;
    }

    public int type()
    {
		return _type;
	}
    
    public ProjectorClient projectorClient()
    {
    	return _projectorClient;
    }
    
}