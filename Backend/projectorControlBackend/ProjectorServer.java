package projectorControlBackend;

import java.util.Calendar;
import java.text.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.Array;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
/*
 * 
 * Implements the backend configuration downloader.  Will grab the configuration and parse it 
 * 
 * 
 * 
 * 
 * 
 * 
 */

public class ProjectorServer implements ProjectorEventListener
{
	private static final long serialVersionUID = 1L;
	private EpsonProjectorControl[] pControl;
	private ProjectorClient[][] projectorClient;
	private SocketListener[] socketListener;
	private Configuration configuration;
	private int logLevel;
	private boolean verbose;
	private String username;
	private String password;
	
	public ProjectorServer( String _configFile, String _username, String _password, 
			int _logLevel, boolean _verbose )
	{
		logLevel = _logLevel;
		verbose = _verbose;
		username = "";
		password = "";
		try
		{
			configuration = new Configuration( _configFile );
		}
		catch( java.io.FileNotFoundException e )
		{
			System.exit(0);
		}
		pControl = new EpsonProjectorControl[configuration.getProjectorCount()];
		socketListener = new SocketListener[configuration.getServerCount()];
		projectorClient = new ProjectorClient[configuration.getServerCount()][];
		for( int i = 0; i < configuration.getProjectorCount(); i++ )
		{
			pControl[i] = new EpsonProjectorControl( 
					configuration.getProjectorConfiguration( i ) );    //create a new object for each connected projector.  
			pControl[i].addProjectorEventListener( this );
			// connect to projector
			pControl[i].communicationStart();
		}
		for( int i = 0; i < configuration.getServerCount(); i++ )
		{
			projectorClient[i] = new ProjectorClient[configuration.
			                                         getProjectorServerConfiguration( i ).
			                                         getMaxClients()];
			if ( !( _username == null ) && !( _password == null ) )
			{
				username = _username;
				password = _password;
			}
			else if ( !( configuration.getProjectorServerConfiguration( i ).getUsername() == null ) && 
					!( configuration.getProjectorServerConfiguration( i ).getPassword() == null ) )
			{
				username = configuration.getProjectorServerConfiguration( i ).getUsername();
				password = configuration.getProjectorServerConfiguration( i ).getPassword();
			}
			socketListener[i] = new SocketListener( 
					configuration.getProjectorServerConfiguration( i ).getServerType(),
					configuration.getProjectorServerConfiguration( i ).getServerPort(),
					projectorClient[i], pControl[0], username, password );
		}
	} // end method ProjectorServer
	public static void main( String[] args )
	{
		boolean verbose = false;
		int logLevel = 0;
		String configFile = "Pconfig.xml";  // contains settings for this projector
		String configServerFile = "PconfigServers.xml";  // contains location of the online Pconfig.xml page
		boolean replaceCached = false; // tells whether to replace the cached version of Pconfig.xml with the downloaded version
		String username = null;
		String password = null;
		String configServerUrl="";
		String temporaryConfigFile = "TempPconfig.xml";  // a place to keep a temporarily downloaded copy of Pconfig.xml
		
		// process command line arguments
		for( int i = 0; i < args.length; i++ )
		{
			int paramStop = args[i].indexOf( "=" );
			String parameter = args[i].substring( 0, paramStop ).trim();
			String argument = args[i].substring( paramStop+1, args[i].length() ).trim();
			System.out.println( "Parameter: " + parameter + "\nArgument: " + argument );
			if( parameter.toLowerCase().equals( "configfile" ) )
			{
				configFile = argument;
			}
			else if( parameter.toLowerCase().equals( "configserverfile" ) )
			{
				configServerFile = argument;
			}
			else if( parameter.toLowerCase().equals( "username" ) )
			{
				username = argument;
			}
			else if( parameter.toLowerCase().equals( "password" ) )
			{
				try
				{
					password = SHA1.getSHA1Hash( argument );
				}
				catch( SHA1.HashException e )
				{
					System.out.println( e.toString() );
					password = "";
				}
			}
			else if( parameter.toLowerCase().equals( "loglevel" ) )
			{
				logLevel = Integer.getInteger( argument );
			}
			else if( parameter.toLowerCase().equals( "verbose" ) )
			{
				verbose = Boolean.getBoolean( argument );
			}
			else if( parameter.toLowerCase().equals( "tempconfigfile" ) )
			{
				temporaryConfigFile =  argument;
			}
		}
		
		// process configServerFile
		try
		{
	        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
	        Document doc = docBuilder.parse( new File( configServerFile ) );
	        doc.getDocumentElement().normalize();
	        if( doc.getDocumentElement().getNodeName().equals( "Pconfig_Servers" ) )
            {
	            NodeList listOfServers = doc.getElementsByTagName( "Pconfig_Server" );
	            int serverCount = listOfServers.getLength();
	            System.out.println ( serverCount + " Servers" );
	            for( int s=0; s<serverCount; s++ )
	            {
	                Node serverNode = listOfServers.item( s );
	                if( serverNode.getNodeType() == Node.ELEMENT_NODE )
	                {
	                	configServerUrl = serverNode.getTextContent() + "?computername=" +
	                		System.getenv( "computername" );
	                	replaceCached = serverNode.
	                			getAttributes().getNamedItem( "cache" ).getNodeValue().
	                			trim().toLowerCase().equals( "true" );
	                }//end of if clause
	            }//end of for loop with s var;
	            System.out.println( "cache=" + replaceCached +"\nurl=" + configServerUrl );
            }
		}
		catch ( java.io.FileNotFoundException e )
		{
			System.out.println( e.toString() );
		}
		catch ( SAXParseException err )
		{
	        System.out.println ( "** Parsing error" + ", line " 
	             + err.getLineNumber () + ", uri " + err.getSystemId ());
	        System.out.println( " " + err.getMessage () );
        }
		catch ( SAXException e )
		{
	        Exception x = e.getException ();
	        ( ( x == null ) ? e : x ).printStackTrace ();
        }
		catch (Throwable t)
		{
			t.printStackTrace ();
        }

		if ( downloadFile( configServerUrl, temporaryConfigFile ) ) // true means successful download
		{
			System.out.println( "Using downloaded configuration" );
			if( replaceCached )
			{
				if( copyFile( temporaryConfigFile, configFile ) )
				{
					System.out.println( "Successfully cached downloaded configuration" );
				}
				else //error copying file
				{
					System.out.println( "There was an error caching the config file" );
				}
			}
			else  // failed attempt at downloading
			{
				System.out.println( "Not caching downloaded configuration" );
			}
			new ProjectorServer( temporaryConfigFile, username, password, logLevel, verbose  );
		}
		else // unsuccessful download
		{
			System.out.println( "Using cached configuration file" );
			new ProjectorServer( configFile, username, password, logLevel, verbose  );
		}
		// System.exit(0);  //may prevent a debug error.  ??  How would this prevent an error?  Looks like the process will just run as a zombie?
		//TODO : Look into this comment.  System.exit(0) is disabled.
	} // end method main
	
	static boolean copyFile( String source, String destination )
	{
		boolean successful = true;
		try
		{
	        InputStream in = new FileInputStream( source );
	        OutputStream out = new FileOutputStream( destination );
	        byte[] buf = new byte[1024];
	        int len;
	        while ( ( len = in.read( buf ) ) > 0 )
	        {
	            out.write( buf, 0, len );
	        }
	        in.close();
	        out.close();
		}
		catch( IOException e )
		{
			System.out.println( e.toString() );
			successful = false;
		}
        return successful;
	}
	
	static boolean downloadFile( String strurl, String filename )
	{
		boolean successful = true;
		OutputStream out = null;
		URLConnection conn = null;
		InputStream  in = null;
		try
		{
			URL url = new URL( strurl );
			out = new BufferedOutputStream( new FileOutputStream( filename ) );
			conn = url.openConnection();
			in = conn.getInputStream();
			byte[] buffer = new byte[1024];
			int numRead;
			successful = in.available() != 0;  // sets successful to false if no bytes are read;
			while ( ( numRead = in.read( buffer ) ) != -1)
			{
				out.write( buffer, 0, numRead );
			}
		}
		catch( MalformedURLException e )
		{
			successful = false;
		}
		catch( IOException e )
		{
			successful = false;
		}
		finally
		{
			try
			{
				if (in != null)
				{
					in.close();
				}
				if (out != null)
				{
					out.close();
				}
			} catch (IOException ioe){}
		}
		return successful;
	}
	private void logEvent( String eventText )
	{
		DecimalFormat twoDigitFormat = new DecimalFormat( "00" );
		Calendar logCalendar = Calendar.getInstance();
		String logDate = String.valueOf( logCalendar.get( Calendar.MONTH ) ) + 
			"/" + String.valueOf( logCalendar.get( Calendar.DAY_OF_MONTH ) ) +
			"/" + String.valueOf( logCalendar.get( Calendar.YEAR ) );
		String logTime = String.valueOf( logCalendar.get( Calendar.HOUR_OF_DAY ) ) + 
		":" + twoDigitFormat.format( logCalendar.get( Calendar.MINUTE ) ) +
		":" + twoDigitFormat.format( logCalendar.get( Calendar.SECOND ) );
		try
		{
			PrintWriter out = new PrintWriter(new FileWriter( "UsageLog.csv", true ) );
			out.println( logDate + "," + logTime + 
				"," + eventText + "," +
				System.getenv( "username" ) );
			out.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
    public void projectorEventReceived( ProjectorEvent projectorEvent )
    {
    	if( projectorEvent.Type().equals( "getInfo" ) )
		{
			 broadcastInfoToClients();
		}
    }
    private void broadcastInfoToClients()
    {
		for( int i = 0; i < Array.getLength( projectorClient[i] ); i++ )
		{
			for( int x = 0; i < Array.getLength( projectorClient[i][x] ); x++ )
			{
				try
				{
					projectorClient[i][x].receiveServerData( "someday there will be data here" );
				}
				catch( NullPointerException e )// nobody home!
				{
					System.out.println( "No local client @ " + Integer.toString( i ) );
				}
			}
		}
    }
} // end class ProjectorServer