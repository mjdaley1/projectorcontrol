package projectorControlBackend;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
 
public class SHA1
{
    private static String convertToHex( byte[] input ) throws HashException
    {
    	StringBuffer out = new StringBuffer();
    	for( int i = 0; i < input.length; i++ )
    	{
    		out.append( byteToHexChars( input[i] ) );
    	}
    	return out.toString();
    }
    private static char[] byteToHexChars( byte input ) throws HashException
    {
    	char[] out = new char[2];
    	out[0] = ( nybbleToHexChar( (input >>> 4 ) & 0x0F ) );
    	out[1] = ( nybbleToHexChar( input & 0x0F ) );
    	return out;
    }
    private static char nybbleToHexChar( int nybble ) throws HashException
    {
    	if( nybble >= 0 && nybble <= 9 )
    	{
    		return (char) ( '0' + nybble );
    	}
    	else if( nybble >= 10 && nybble <= 15 )
    	{
    		return (char) ( 'a' + nybble-10 );
    	}
    	else
    	{
    		throw new HashException();
    	}
    }
    public static String getSHA1Hash( String input ) throws HashException
    {
		MessageDigest md = null;
		byte[] hash = new byte[40];
		try
		{
			md = MessageDigest.getInstance( "SHA-1" );
		}
		catch( NoSuchAlgorithmException e )
		{
			throw new HashException();
		}
		try
		{
			md.update( input.getBytes( "US-ASCII" ), 0, input.length() );
		}
		catch( UnsupportedEncodingException e )
		{
			throw new HashException();
		}
		hash = md.digest();
		return convertToHex( hash );
    }
    
    static class HashException extends Exception
    {
    	private static final long serialVersionUID = 1L;
    	private String error;

    	public HashException()
    	{
    		super();
    		error = "SHA1 hash error";
    	}
    	public String getError()
    	{
    		return error;
    	}
    }
}
