; Taken from http://nsis.sourceforge.net/Simple_installer_with_JRE_check by weebib
; Use it as you desire.
 
;TODO:  Add simple service plugin to this to remove the need for executing a batch script.  
 
; Credit given to so many people of the NSIS forum.
;***************************************************
;EXPORT THE JAR as the ShortName of the Application!
;*************************************************** 

!define AppName "CCRI Projector Control"
!define AppVersion "2.3"
!define ShortName "Backend"
!define Vendor "CCRI"
;***************
!addPluginDir nsisplugins/service ;add the service plugin to the compiler
!include "MUI.nsh"
!include "Sections.nsh"
;------------

;Attributes
SilentInstall	silent
SilentUnInstall 'silent'

;--------------------------------
;Configuration
 
 ;RequestExecutionLevel admin   ;is this causing issues?
 
  ;General
  Name "${AppName} ${ShortName}"
  OutFile "build\CCRIProjectorControlBackend-setup.exe"
 
  ;Folder selection page
  InstallDir "$PROGRAMFILES\${APPNAME}\${SHORTNAME}"
 
  ;Get install folder from registry if available
  InstallDirRegKey HKLM "SOFTWARE\${Vendor}\${AppName}\${ShortName}" ""
 
  Icon "ccriicon.ico"
  UninstallIcon "ccriuninst.ico"
; Installation types
;InstType "full"    ; Uncomment if you want Installation types
 
 ;Function .onInit
 
;Run the uninstaller, when launching the installer.  This does not prompt at all.  It just happens.... be sure your ready to uninstall...
;this should probably prompt the user before doing this on a graphical install.  Otherwise, just do it....
 ; ClearErrors
 ;Exec $INSTDIR\uninstall.exe ; instead of the ExecWait line
 ;ExecWait '"$INSTDIR\Uninstall.exe" /S _?=$INSTDIR'
 
;FunctionEnd
 
 
 
;--------------------------------
;Pages
Page components
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles
;--------------------------------
;Modern UI Configuration
 
  !define MUI_ABORTWARNING
 
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"
 
;--------------------------------
;Language Strings
 
  ;Description
  LangString DESC_SecAppFiles ${LANG_ENGLISH} "Application files"
 
  ;Header
  LangString TEXT_PRODVER_TITLE ${LANG_ENGLISH} "Installed version of ${AppName}"
  LangString TEXT_PRODVER_SUBTITLE ${LANG_ENGLISH} "Installation cancelled"
 
;--------------------------------
;Installer Sections
Section "Installation of ${AppName} ${ShortName}" SecAppFiles
  SectionIn 1 RO    ; Full install, cannot be unselected
            ; If you add more sections be sure to add them here as well
  
  ;Remove Previous Versions folder Just in Case...  Should already be uninstalled...,  stop the service too..
  
  
  ;;;;THIS IS NOT A GOOD IDEA!!!!  What if this file is not found...  The backend would keep on running preventing the new installer from completing...
  ;ExecWait '"$INSTDIR\UninstallBackend-NT.bat"'  
  
  ;Uninstall Previous version
  ExecWait '"$INSTDIR\Uninstall.exe" /S _?=$INSTDIR'
  RMDir /r "$INSTDIR\${APPNAME}"
  ;Remove stray registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}"
  DeleteRegKey HKLM "SOFTWARE\${Vendor}\${AppName} ${ShortName}"

  ;Install the files
  SetOutPath $INSTDIR

  File "build\backend.jar"
  File "build\changelog.txt"
  File "PconfigServers.xml"
  File "ccriicon.ico"
  File "ccriuninst.ico"
  File "ccri.png"
  FileOpen $4 "Pconfig.xml" w
  FileClose $4
  
  WriteRegStr HKLM "SOFTWARE\${Vendor}\${AppName} ${ShortName}" "" $INSTDIR
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}" "DisplayName" "${AppName} ${ShortName}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}" "UninstallString" '"$INSTDIR\Uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}" "NoModify" "1"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}" "NoRepair" "1"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}" "Publisher" "${VENDOR}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}" "DisplayVersion" "${AppVersion}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}""DisplayIcon" "$INSTDIR\ccriuninst.ico"
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
 
SectionEnd

Section "JRE7"
    SectionIn 1
    SetOutPath $INSTDIR
   File /r wrapper\jre7
SectionEnd

Section "RXTX Serial Libraries" 
    SectionIn 1 
    SetOutPath $INSTDIR\lib
    File "rxtxSerial.dll"
    SetOutPath $INSTDIR\lib\ext
    File "RXTXComm.jar"
SectionEnd

Section "NT Service Wrapper"
    SectionIn 1
    SetOutPath $INSTDIR\lib
    File "wrapper\lib\wrapper.dll"
    File "wrapper\lib\wrapper.jar"
    SetOutPath $INSTDIR
    File "wrapper\InstallBackend-NT.bat"
    File "wrapper\UninstallBackend-NT.bat"
    File "wrapper\wrapper.conf"
    File "wrapper\wrapper.exe"
    File "wrapper\wrapperlicense.txt"
    File "wrapper\StartBackend-NT.bat"
    ;SimpleSC::InstallService "Projector Control Backend" "Projector Control Backend" "16" "2" "$INSTDIR\wrapper.exe -s $INSTDIR\wrapper.conf" "" "" ""
	;Pop $0
	;SimpleSC::StartService "Projector Control Backend"
	ExecWait '"$INSTDIR\InstallBackend-NT.bat"'
    ExecWait '"$INSTDIR\StartBackend-NT.bat"'
SectionEnd
 
 
;create shortcut to the restart-backend.bat script.  **** ONLY FOR THE FRONTEND INSTALLER
;!!!!!If installing shortcuts, make sure to set the shell context.  It will install to the user SMPROGRAMS dir otherwise.

;--------------------------------
;Uninstaller Section
Section "Uninstall"
  ; remove shortcuts, if any.
  ;Make sure to set shell context to remove this for all users.  Must be set for uninstall and install.
  ;Delete "$SMPROGRAMS\${AppName}\*.*"
   ; Remove NT Service
  ExecWait '"$INSTDIR\UninstallBackend-NT.bat"'
  ; remove files   this is scary and needs to be changed...  what if this registry key is missing?!
  RMDir /r "$INSTDIR"
  ;check if frontend dir exists.  If it does, DO NOTHING. It if does not, remove the program folder.
   IfFileExists $INSTDIR\Frontend +2 0
    RMDir "$PROGRAMFILES\${AppName}"
  ; remove registry keys.  Do this last!
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}"
  DeleteRegKey HKLM  "SOFTWARE\${Vendor}\${AppName} ${ShortName}"
  
  ;Add some logging here to detect if the installer/uninstaller finished successfully.
SectionEnd