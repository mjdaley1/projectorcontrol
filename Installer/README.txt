MJD 9-6-2014
This installer has been modified using ORCA to remove the extra tag that MSI Wrapper adds to the add remove programs name.  Also an additional upgrade code was added to remove the backend and frontend MSI's of the previous installation.  

{632bdf19-6c9a-480b-a572-6d9971897de9}      frontend upgrade code
{eed3b6d4-af3a-4831-93c4-5d55b6edce12}		backend upgrade code

Version Numbers are removed in the upgrade table which forces a major upgrade/downgrade no matter what version is already installed... :)  Might have an issue if I rerelease with a different product code...

Enjoy.
