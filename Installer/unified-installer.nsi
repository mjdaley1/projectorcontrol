; Credit given to so many people of the NSIS forum.
;***************************************************
;EXPORT THE JAR's as the ShortName of the Application!
;backend, and frontend respectively.
;*************************************************** 

!define AppName "CCRI Projector Control"
!define AppVersion "2.3.0.1"
!define Vendor "CCRI"
!define JREversion "1.7.0" ;This is used to append the version number to path for the installer.
!define JavaPath	"$INSTDIR\Java\jre\bin\javaw.exe"
;***************
!addPluginDir .\nsisplugins\dll ;add the service plugin to the compiler and others?
!include "MUI.nsh"
!include "Sections.nsh"
;------------

;Attributes
SilentInstall	silent
SilentUnInstall 'silent'

;--------------------------------
;Configuration
 
 ;RequestExecutionLevel admin   ;is this causing issues?
 
  ;General
  Name "${AppName}"
  OutFile "CCRIProjectorControl-setup - ${AppVersion}.exe"
 
  ;Folder selection page
  InstallDir "$PROGRAMFILES\${APPNAME}"
 
  ;Get install folder from registry if available.  Not reallly wanted for the silent install.  This could cause issues..
  ;InstallDirRegKey HKLM "SOFTWARE\${Vendor}\${AppName}" "Install_Dir"
 
  Icon "..\Backend\ccriicon.ico"
  UninstallIcon "..\Backend\ccriicon.ico"
; Installation types
;InstType "full"    ; Uncomment if you want Installation types

;--------------------------------
;Pages
Page components
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles
;--------------------------------
;Modern UI Configuration
 
  !define MUI_ABORTWARNING
 
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"
 
;--------------------------------
;Language Strings
 
  ;Description
  LangString DESC_SecAppFiles ${LANG_ENGLISH} "Application files"
 
  ;Header
  LangString TEXT_PRODVER_TITLE ${LANG_ENGLISH} "Installed version of ${AppName}"
  LangString TEXT_PRODVER_SUBTITLE ${LANG_ENGLISH} "Installation cancelled"
 
;--------------------------------
Function .onInit
;Uninstaller for the old version.  Keep this outside of the installation page.  .onInit
 ;Remove the existing service if any...,  waits for file release, timeout 30
  SimpleSC::StopService "Projector Control Backend" 1 30
  SimpleSC::RemoveService "Projector Control Backend" 
  Pop $0
  
  ;Uninstall Previous version.  _?=$INSTDIR waits for the uninstaller to finish.
removeUnified: 
IfFileExists "$INSTDIR\Uninstall.exe" 0 +2
  ExecWait '"$INSTDIR\Uninstall.exe" /S _?=$INSTDIR'
  Sleep 500
  
removeBackend:
IfFileExists "$PROGRAMFILES\${APPNAME}\Backend\Uninstall.exe" 0 +2
  ExecWait '"$PROGRAMFILES\${APPNAME}\Backend\Uninstall.exe" /S _?=$PROGRAMFILES\${APPNAME}\Backend'
  Sleep 500

removeFrontend:
  IfFileExists "$PROGRAMFILES\${APPNAME}\Frontend\Uninstall.exe" 0 +2
  ExecWait '"$PROGRAMFILES\${APPNAME}\Frontend\Uninstall.exe" /S _?=$PROGRAMFILES\${APPNAME}\Frontend'
  Sleep 500
  
removeDirectory: 
	;Just for good measure.
  RMDir /r "$PROGRAMFILES\${APPNAME}"
  
  ;Remove stray registry keys, if they exist...
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\CCRI Projector Control Backend"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\CCRI Projector Control Frontend"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\CCRI Projector Control"

FunctionEnd

;Installer Sections
Section "Installation of ${AppName}" SecAppFiles
  SectionIn 1 RO    ; Full install, cannot be unselected
            ; If you add more sections be sure to add them here as well
  
  ;Set install path in the registry
  WriteRegStr HKLM "SOFTWARE\${Vendor}\${AppName}" "Install_Dir" "$INSTDIR"
  
  ;Install the backend files
  SetOutPath "$INSTDIR\Backend"

  File "..\Backend\build\backend.jar"
  File "..\Backend\build\changelog.txt"
  File "..\Backend\PconfigServers.xml"
  File "..\Backend\ccriicon.ico"
  File "..\Backend\ccriuninst.ico"
  File "..\Backend\ccri.png"
  FileOpen $4 "Pconfig.xml" w
  FileClose $4
  
 ;Write add/remove programs registry entries
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName}" "DisplayName" "${AppName}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName}" "UninstallString" '"$INSTDIR\Uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName}" "NoModify" "1"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName}" "NoRepair" "1"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName}" "Publisher" "${VENDOR}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName}" "DisplayVersion" "${AppVersion}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName}" "DisplayIcon" "$INSTDIR\ccriuninst.ico"
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
 
SectionEnd

Section "JRE"
    SectionIn 1
    SetOutPath $INSTDIR\Java
;Match all files recursively under the named folder.
	File /r "..\Java\jre"
SectionEnd

Section "RXTX Serial Libraries"    ;Start to look into making this platform dependant.  Wont need these on OSX, or 64bit ones for 64bit OS.
    SectionIn 1 
    SetOutPath $INSTDIR\Backend\lib
    File "..\Backend\rxtxSerial.dll"
    SetOutPath $INSTDIR\Backend\lib\ext
    File "..\Backend\RXTXComm.jar"
SectionEnd

Section "NT Service Wrapper"
    SectionIn 1
    SetOutPath $INSTDIR\Backend\lib
    File "..\Backend\wrapper\lib\wrapper.dll"
    File "..\Backend\wrapper\lib\wrapper.jar"
    SetOutPath $INSTDIR\Backend
    File "..\Backend\wrapper\InstallBackend-NT.bat"
    ;File "..\Backend\wrapper\UninstallBackend-NT.bat"  ;no longer needed
    File "..\Backend\wrapper\wrapper.conf"
    File "..\Backend\wrapper\wrapper.exe"
    File "..\Backend\wrapper\wrapperlicense.txt"
    File "..\Backend\wrapper\StartBackend-NT.bat"
	;Install the projector control service.  "16" = run as own process.  "2"=auto-start.  "Path to exe". "" = no depends, "" = account. "" = account pass.  (RUN AS SYSTEM)
    ;SimpleSC::InstallService "Projector Control Backend" "Projector Control Backend" "16" "2" '$INSTDIR\Backend\wrapper.exe -s $INSTDIR\Backend\wrapper.conf' "" "" ""
	;Pop $0
	
	
	
	;Install the Service.  The bat script is quicker! 
	ExecWait '"$INSTDIR\Backend\InstallBackend-NT.bat"'
	;ExecWait `"$INSTDIR\Backend\wrapper.exe" -i "$INSTDIR\Backend\wrapper.conf"`
	
	;Start service, timeout of 30 seconds.
	;SimpleSC::StartService "Projector Control Backend" "" 30
	;Pop $0
	
	;Install and start the backend service
	
    ExecWait '"$INSTDIR\Backend\StartBackend-NT.bat"'
SectionEnd
 
 Section "Frontend"
 SectionIn 1
 ;Install the files
  SetOutPath $INSTDIR\Frontend

  File "..\Frontend\build\frontend.jar"
  File "..\Frontend\build\changelog.txt"
  File "..\Frontend\ccriicon.ico"
  File "..\Frontend\ccriuninst.ico"
  File "..\Frontend\ccri.png"
  File "..\Frontend\restart-backend.bat"
  ;File "..\Frontend\UsageLog.csv"
  ;Create a new usagelog on each machine install.
  FileOpen $4 "UsageLog.csv" w
  FileClose $4
  
  
  CreateShortCut "$INSTDIR\Frontend\${AppName}.lnk" '"${JavaPath}"' '-jar "$INSTDIR\Frontend\frontend.jar"' "$INSTDIR\Frontend\ccriicon.ico"

  ;Place shortcuts in the all users folder.
  SetShellVarContext all
  RMDir /r "$SMPROGRAMS\${AppName}"
  CreateDirectory "$SMPROGRAMS\${AppName}"
  CreateShortCut "$SMPROGRAMS\${AppName}\${AppName}.lnk" '"${JavaPath}"' '-jar "$INSTDIR\Frontend\frontend.jar"' "$INSTDIR\Frontend\ccriicon.ico"
 
 ;write the registry key that makes the frontend start with every login.
 WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Run" "${AppName} Frontend" '"$INSTDIR\Frontend\${AppName}.lnk"'
 
 ;Add the frontend files here..  This is all about simplification.
 SectionEnd
 
;create shortcut to the restart-backend.bat script.  **** ONLY FOR THE FRONTEND INSTALLER
;!!!!!If installing shortcuts, make sure to set the shell context.  It will install to the user SMPROGRAMS dir otherwise.

;--------------------------------
;Uninstaller Section
Section "Uninstall"
  ;Make sure to set shell context to remove this for all users.  Must be set for uninstall and install.
  SetShellVarContext all
  RMDir /r "$SMPROGRAMS\${AppName}"
  
  SimpleSC::StopService "Projector Control Backend" 1 30
  Pop $0 ;returns an errorcode unless it is successful (0)
  
  ;Remove the service
  ExecWait "$INSTDIR\Backend\wrapper.exe -r $INSTDIR\Backend\wrapper.conf"
  
  ;Just in case....
  SimpleSC::RemoveService "Projector Control Backend"
  Pop $0
  
  ; remove files using the program files variable and appname.  This is better than InstallDir since it does not depend on the registry key :)
  RMDir /r "$PROGRAMFILES\${AppName}"
  ;check if frontend dir exists.  If it does, DO NOTHING. It if does not, remove the program folder.
  ; IfFileExists $INSTDIR\Frontend +2 0
  ; RMDir "$PROGRAMFILES\${AppName}"
  ; remove registry keys.  Do this last!
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName}"
  DeleteRegKey HKLM  "SOFTWARE\${Vendor}\${AppName}"
  
  ;Add some logging here to detect if the installer/uninstaller finished successfully.
SectionEnd