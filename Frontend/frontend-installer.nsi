;***************************************************
;EXPORT THE JAR as the ShortName of the Application!
;*************************************************** 

!define AppName "CCRI Projector Control"
!define AppVersion "2.2.3.0"
!define ShortName "Frontend"
!define Vendor "CCRI"
;------------------------------------
;java defines
!define JRE_VERSION "1.6"
!define JRE_URL "http://javadl.sun.com/webapps/download/AutoDL?BundleId=49038"
;---------------------------------------

!include "MUI.nsh"
!include "Sections.nsh"

!include LogicLib.nsh
!include UAC.nsh
 
;--------------------------------
;Configuration
 
 ;RequestExecutionLevel admin
 
 ;This installer and uninstaller are completely silent!! Beware
 SilentInstall silent
 SilentUnInstall silent
  ;General
  Name "${AppName} ${ShortName}"
  OutFile "build\CCRIProjectorControl${ShortName}-setup.exe"
 
  ;Folder selection page
  InstallDir "$PROGRAMFILES\${APPNAME}\${SHORTNAME}"
 
  ;Get install folder from registry if available
  InstallDirRegKey HKLM "SOFTWARE\${Vendor}\${AppName}\${ShortName}" ""
 
  Icon "ccriicon.ico"
  UninstallIcon "ccriicon.ico"
  
Function .onInit

;!insertmacro UAC_RunElevated

;Run the uninstaller, when launching the installer.  This does not prompt at all.  It just happens.... be sure your ready to uninstall...
;this should probably prompt the user before doing this on a graphical install.  Otherwise, just do it....
  ;ClearErrors
 ;Exec $INSTDIR\uninstall.exe ; instead of the ExecWait line  
 ;Silently run the uninstaller.
 ;ExecWait '"$INSTDIR\Uninstall.exe" /S _?=$INSTDIR'
 
FunctionEnd
  
  
  
  
; Installation types
;InstType "full"    ; Uncomment if you want Installation types
 
;--------------------------------
;Pages
Page components
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles
;--------------------------------
;Modern UI Configuration
 
  !define MUI_ABORTWARNING
 
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"
 
;--------------------------------
;Language Strings
 
  ;Description
  LangString DESC_SecAppFiles ${LANG_ENGLISH} "Application files"
 
  ;Header
  LangString TEXT_PRODVER_TITLE ${LANG_ENGLISH} "Installed version of ${AppName}"
  LangString TEXT_PRODVER_SUBTITLE ${LANG_ENGLISH} "Installation cancelled"
 
;--------------------------------
;Installer Sections
Section "Installation of ${AppName} ${ShortName}" SecAppFiles

  SectionIn 1 RO    ; Full install, cannot be unselected
            ; If you add more sections be sure to add them here as well
  
 
;Remove old folder to eliminate any old messy stuff..  Let's do an uninstall!
 RMDir /r "$INSTDIR\${APPNAME}\"
 DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}"
 DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Run" "${AppName} ${ShortName}"
 DeleteRegKey HKLM "SOFTWARE\${Vendor}\${AppName} ${ShortName}"
 Delete "$SMPROGRAMS\${AppName}\*.*"
 RMDir /r "$SMPROGRAMS\${AppName}"
 
  ;Install the files
  SetOutPath $INSTDIR

  File "build\frontend.jar"
  File "build\changelog.txt"
  File "ccriicon.ico"
  File "ccriuninst.ico"
  File "ccri.png"
  File "restart-backend.bat"
  File "UsageLog.csv"
  
  CreateShortCut "$INSTDIR\${APPNAME} ${SHORTNAME}.lnk" "$PROGRAMFILES\${APPNAME}\Backend\jre7\bin\javaw.exe" "-jar $\"$INSTDIR\frontend.jar$\"" '$INSTDIR\ccriicon.ico'
  
  WriteRegStr HKLM "SOFTWARE\${Vendor}\${AppName} ${ShortName}" "" $INSTDIR
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}" "DisplayName" "${AppName} ${ShortName}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}" "NoModify" "1"
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}" "NoRepair" "1"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}" "Publisher" "${VENDOR}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}" "DisplayVersion" "${AppVersion}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}""DisplayIcon" "$INSTDIR\ccriicon.ico"
 
 ;write the registry key that makes the frontend start with every login.
 WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Run" "${AppName} ${ShortName}" '"$INSTDIR\${AppName} ${ShortName}.lnk"'

 ;Create uninstaller
 WriteUninstaller "$INSTDIR\Uninstall.exe"
 
SectionEnd

Section "Start menu shortcuts" SecCreateShortcut
  SectionIn 1   ; Can be unselected
  ;First remove any old and stale shortcuts.....
  SetShellVarContext all
  RMDir /r "$SMPROGRAMS\${AppName}"
  CreateDirectory "$SMPROGRAMS\${AppName}"
  CreateShortCut "$SMPROGRAMS\${AppName}\${AppName} ${ShortName}.lnk" "$PROGRAMFILES\${AppName}\Backend\jre7\bin\javaw.exe" "-jar $\"$INSTDIR\frontend.jar$\"" '$INSTDIR\ccriicon.ico'
 ;create shortcut to the restart-backend.bat script.  **** ONLY FOR THE FRONTEND INSTALLER
  ;CreateShortCut "$SMPROGRAMS\${AppName}\${AppName} Backend Restart.lnk" "$INSTDIR\restart-backend.bat"
SectionEnd
 

;--------------------------------
;Uninstaller Section
Section "Uninstall"
  SetShellVarContext all
  ; remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${AppName} ${ShortName}"
  DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Run" "${AppName} ${ShortName}"
  DeleteRegKey HKLM "SOFTWARE\${Vendor}\${AppName} ${ShortName}"
  ; remove shortcuts, if any.
  Delete "$SMPROGRAMS\${AppName}\*.*"
  RMDir /r "$SMPROGRAMS\${AppName}"
  ; remove files
  RMDir /r "$INSTDIR"
   ;check if backend dir exists.  If it does, DO NOTHING. It if does not, remove the program folder.+2 means skip two lines.
   IfFileExists $INSTDIR\Backend +2 0
   RMDir "$PROGRAMFILES\${AppName}"

SectionEnd