

package projectorControlFrontend;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.EventObject;
import java.util.Hashtable;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.BorderUIResource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class ProjectorControlFrontend extends JFrame implements StatusEventListener
{
	private final String VERSION = "2.4.0.0";
	private Hashtable<String, String> contactNumbers;
	
	private static final long serialVersionUID = 1L;
	
	// initialize objects and variables
	private JButton projectorDisplayCloneButton;
	private JButton projectorPowerJButton;
	private JButton projectorStandbyJButton;
	private JButton projectorHelpButton;
	private InputJButton inputJButton[];
	
	private JTextArea projectorStatusJTextArea;
	
	private JLabel projectorVolumeJLabel;
	private JSlider projectorVolumeJSlider;
	
	private ProjectorControlSocket projectorControlSocket;
	
	//create input Holding variables
	private int requestedInput = -1;  //make sure this is not a valid input, or it will startup bold.
	
	// Stored connection details
	private String storedAddressPort;
	private String storedUser;
	private String storedPass;
	
	private int volumeLock;		// Expected volume lock value
	final private boolean FORCE_VOLUME_ACQUIRE = false;	// Causes the client to become forceful
	private static Process p;
														// when waiting for the server's volume
														// to match the expected. Helps break
														// deadlocks if more than one client is
														// connected to a server.
	
    public ProjectorControlFrontend( String serverAddressPort, String user, String pass )
    {
		setDefaultCloseOperation( JFrame.DO_NOTHING_ON_CLOSE );
		System.out.println("Adding shutdown hook thread");
		Runtime.getRuntime().addShutdownHook( new Thread()
        {
            public void run() 
            {
                
            	cleanShutdown();
            	System.out.println("Halting...");
                Runtime.getRuntime().halt(0);  //this prevents a locked application on shutdown.  Needed as of Java6update23?
                System.exit(0);
            }
            
        } );
       
		setResizable( false );
		//Set the initial location of the interface.  DO NOT BLOCK Desktop icons.
		//TODO: Get current screen size of this display/displays.
		//setLocation(250,0);
		
		
		// Set the expected volume lock to the "unlocked" value
		volumeLock = -1;
		
		
		// Connect to the backend
		boolean connected = setupControlSocket( serverAddressPort, user, pass );
		if( connected )
		{
			// create the interface according to info
			createUserInterface( projectorControlSocket.configurationInfo );
			setContactNumbers();
			Log.logEvent( "Interface Launched" );
			displayClone();   //Clone the displays on starting up the frontend.  This will only execute if the connection is completed.   
		}
		else
		{
			projectorControlSocket.close();
			System.out.println( "Could not contact server" );
		}
    }
    
    
    private void setContactNumbers()
    {
    	contactNumbers = new Hashtable<String, String>( 3 );
    	contactNumbers.put("FC", "401.333.7081");
    	contactNumbers.put("KC", "401.825.2231");
    	contactNumbers.put("LC", "401.455.6120");
    }
    
    
    /**
     * Sets up and stores a new ProjectorControlSocket object
     * @return If connection was successful
     */
    private boolean setupControlSocket( String serverAddressPort, String user, String pass )
    {
    	boolean connected = false;
    	// Connect to backend
    	projectorControlSocket = new ProjectorControlSocket( serverAddressPort, user, pass, this ); 
		// get info from backend
		// wait for info
		long timeoutTime = System.currentTimeMillis() + 15000; // 15 seconds;
		while( projectorControlSocket.configurationInfo == null && 
				timeoutTime > System.currentTimeMillis() )
		{
			try
			{
				Thread.sleep( 200 );
			}
			catch( InterruptedException e ){}
		}
		if( projectorControlSocket.configurationInfo != null )
		{
			connected = true;
			
			// Store the working connection details
			storedAddressPort = serverAddressPort;
			storedUser = user;
			storedPass = pass;
		}
		
		return connected;
    }
    
    
	// create and position GUI components; register event handlers
	private void createUserInterface( ConfigurationInfo configurationInfo )
	{
	//if(projectorControlSocket.configurationInfo.getPort().equalsIgnoreCase("none"))  //might be better to run this outside of the method.
		
		// get content pane for attaching GUI components
		Container contentPane = getContentPane();

		// enable explicit positioning of GUI components
		contentPane.setLayout( null );

		// set up projectorOnOffJButton
		projectorPowerJButton = new JButton();
		projectorPowerJButton.setBounds( 20, 20, 420, 25 );
		projectorPowerJButton.setText( "Please Wait" );
		projectorPowerJButton.setFont( new Font( "SansSerif", Font.PLAIN, 14 ) );
		projectorPowerJButton.setEnabled( true );
		projectorPowerJButton.setBackground( new Color( 111, 255, 120 ) );
		projectorPowerJButton.setBorder( 
				BorderUIResource.getRaisedBevelBorderUIResource() );
		contentPane.add( projectorPowerJButton );
		projectorPowerJButton.addActionListener(
		new ActionListener() // anonymous inner class
		{
			// called when projectorPowerJButton is pressed
			public void actionPerformed( ActionEvent event)
			{
				projectorPowerJButtonActionPerformed( event );
			}
		} // end to anonymous inner class
		); // end call to addActionListener

		// set up projectorStandbyJButton
		projectorStandbyJButton = new JButton();
		projectorStandbyJButton.setBounds( 20, 60, 200, 25 );
		projectorStandbyJButton.setText( "Set Projector to Standby" );
		projectorStandbyJButton.setFont( new Font( "SansSerif", Font.PLAIN, 14 ) );
		projectorStandbyJButton.setEnabled( false );
		projectorStandbyJButton.setBackground( new Color( 255, 255, 68 ) );
		projectorStandbyJButton.setBorder( 
				BorderUIResource.getRaisedBevelBorderUIResource() );
		contentPane.add( projectorStandbyJButton );
		projectorStandbyJButton.addActionListener(
		new ActionListener() // anonymous inner class
		{
			// called when projectorStandbyJButton is pressed
			public void actionPerformed( ActionEvent event)
			{
				projectorStandbyJButtonActionPerformed( event );
			}
		} // end to anonymous inner class
		); // end call to addActionListener

		// set up inputJButton[]
		inputJButton = new InputJButton[ configurationInfo.getInputCount() ];
		for( int i=0; i < inputJButton.length; i++ )
		{
			inputJButton[i] = new InputJButton();
			inputJButton[i].setInputID(i);
			inputJButton[i].setText( configurationInfo.getInputName( i ) );
			inputJButton[i].setFont( new Font( "SansSerif", Font.PLAIN, 14 ) );
			inputJButton[i].setEnabled( false );
			inputJButton[i].setBackground( new Color( 138, 205, 209 ) );
			inputJButton[i].setBounds( 240, 60 + ( 40 * i ), 200, 25 );
			inputJButton[i].setBorder( 
					BorderUIResource.getRaisedBevelBorderUIResource() );
			contentPane.add( inputJButton[i] );
			inputJButton[i].addActionListener(
			new ActionListener() // anonymous inner class
			{
				// called when inputJButton[i] is pressed
				public void actionPerformed( ActionEvent event )
				{
					inputJButtonActionPerformed( event );
				}
			} // end to anonymous inner class
			); // end call to addActionListener
		} // end loop with i var

		// set up projectorStatusJTextArea
		projectorStatusJTextArea = new JTextArea();
		projectorStatusJTextArea.setBounds( 80, 100, 140, 65 );
		projectorStatusJTextArea.setEditable( false );
		projectorStatusJTextArea.setText( "" );
		projectorStatusJTextArea.setOpaque( false );
		projectorStatusJTextArea.setBorder( 
				BorderUIResource.getLoweredBevelBorderUIResource() );
		contentPane.add( projectorStatusJTextArea );
		if( configurationInfo.hasSound() )
		{
			// set up projectorVolumeJLabel
			projectorVolumeJLabel = new JLabel();
			projectorVolumeJLabel.setBounds( 20, 90, 50, 10 );
			projectorVolumeJLabel.setFont( new Font( "SansSerif", Font.PLAIN, 10 ) );
			projectorVolumeJLabel.setText( "Volume" );
			contentPane.add( projectorVolumeJLabel );
	
			// set up projectorVolumeJSlider
			projectorVolumeJSlider = new JSlider( JSlider.VERTICAL, 0, 20, 0);
			projectorVolumeJSlider.setBounds( 20, 100, 50, 65 );
			projectorVolumeJSlider.setMinorTickSpacing(10);
			projectorVolumeJSlider.setPaintTicks( true );
			projectorVolumeJSlider.setPaintLabels( true );
			projectorVolumeJSlider.setEnabled( true );
			projectorVolumeJSlider.setOpaque( false );
			projectorVolumeJSlider.setForeground( Color.BLACK );
			contentPane.add( projectorVolumeJSlider );
	
			projectorVolumeJSlider.addMouseListener( new MouseListener() 
				{
					public void mouseClicked( MouseEvent e ){}
					public void mousePressed( MouseEvent e ){}
				    public void mouseEntered( MouseEvent e ){}
				    public void mouseExited( MouseEvent e ){}
					public void mouseReleased( MouseEvent e )
					{	
						// Volume locking to reduce the chances of a
						// status update overriding the slider value
						volumeLock = projectorVolumeJSlider.getValue();
						projectorControlSocket.sendCommand( "volume " + 
								String.valueOf( volumeLock ));
						Log.logEvent( "Volume Adjusted using Interface" );
					}
				}
			
			);
		}
		//Create a help button. 
		//TODO:  This should launch a web page using the default web browser.  That page will contain documentation for the app.  Use the Desktop#Browse(URI) method.
		projectorHelpButton = new JButton();
		//projectorHelpButton.setBounds( 80, 180, 320, 25 );  //this is centered.
		projectorHelpButton.setBounds(20,180, 200, 25); //left alligned.
		projectorHelpButton.setText( "Help Options" );
		projectorHelpButton.setFont( new Font( "SansSerif", Font.PLAIN, 14 ) );
		projectorHelpButton.setEnabled( true );
		projectorHelpButton.setBackground( new Color( 111, 255, 120 ) );
		projectorHelpButton.setBorder( 
				BorderUIResource.getRaisedBevelBorderUIResource() );
		contentPane.add( projectorHelpButton );
		projectorHelpButton.addActionListener(
		
				new ActionListener() // anonymous inner class
		{
			// called when projectorHelpButton is pressed
			public void actionPerformed( ActionEvent event)
			{
				projectorHelpButton( event );
			}
		} // end to anonymous inner class
		); // end call to addActionListener
		
		
	    FadePanel fp = new FadePanel();   //what is the fade panel?
	    contentPane.add( fp, BorderLayout.CENTER );
	    fp.setSize( 470, 215 );
	    fp.setVisible( true );

	    addWindowListener(
	    new WindowHandler()  //invoke the class to handle events from the window.  Such
	    	{
	    			public void windowClosing( WindowEvent event )
	    			{
	   					//minimizeWindow();
	    				toBack(); //send the window to the back instead of minimizing.  
	   					Log.logEvent( "Window sent to back using close button." );
	    			}
	    			public void windowIconified (WindowEvent event )
	    			{
	    				
	    				setExtendedState(NORMAL);  // this sets the normal state.  prevents iconify.
	    				//JOptionPane.showMessageDialog(getComponent(0), "Message"); //Use this to send a message to the user.  getComponent(0) grabs the parent component i guess.
	    				
	    				Log.logEvent("Window iconify button disabled!.");
	    				
	    			}
	    			public void windowDeiconified(WindowEvent event)  //this is called right after Iconify...
	    			{
	    				//setLocation(250,0);  //lets try it....
	    				centerUI();
	    			}
	    });
	    
		// set final properties of application's window
		setTitle( "CCRI Projector Control - Room: " 
				+ configurationInfo.getClassroom() ); // set title bar string
		contentPane.setBackground( Color.WHITE );
		setSize( 470, 240 );          // set window size  (470,215) original size
		setIconImage( new ImageIcon( "ccri.png" ).getImage());
		setVisible( true );           // display window
		centerUI();   					//run the centering method.  This will center the UI based on current screen size.  
		System.gc();				//run garbage collector.
		
	} // end method createUserInterface
	
	public void statusEventReceived( StatusEvent event )
	{
		while( !this.isVisible() )
		{
			try
			{
				Thread.sleep( 100 );
			}
			catch( InterruptedException e ){}
		}
		changeState( projectorControlSocket.status.getState() );  //Make the interface reflect the current state.

	}
	
	private void changeState( int state )
	{
		switch( state )
		{
		case 1:
			 // powering on state.   Since we are allowing input requests during power on, we need to control
			//the feedback during this time also.  Only keep the bold text for teh requested input during this time. 
			//Copies the code from the case 2 if block.
			projectorStatusJTextArea.setText( "Please Wait \n\nTurning Projector ON");
			for( int i = 0; i < inputJButton.length; i++ )
			{
				inputJButton[i].setEnabled( true );  //Keep the input buttons enabled during power on ONLY!
				if(requestedInput == i)
				{
					inputJButton[i].setFont( new Font( "SansSerif", Font.BOLD, 14 ) );
				}
				else
				{
					inputJButton[i].setFont( new Font( "SansSerif", Font.PLAIN, 14 ) );
					inputJButton[i].setBackground( new Color( 138, 205, 209 ) );
				}
			}
			projectorStandbyJButton.setEnabled( false );
			projectorPowerJButton.setEnabled( false );
			projectorVolumeJSlider.setEnabled( true );
			break;  
		case 3:
			//add a cooling down state to disable the inputs during power off.
			projectorStatusJTextArea.setText( "Please Wait \n\nTurning Projector OFF");
			for( int i = 0; i < inputJButton.length; i++ )  //disable input buttons and change styling back
			{
				inputJButton[i].setEnabled( false );
				inputJButton[i].setFont( new Font( "SansSerif", Font.PLAIN, 14 ) );
				inputJButton[i].setBackground( new Color( 138, 205, 209 ) );  //non active input should be light blue
			}
			projectorStandbyJButton.setEnabled( false );
			projectorPowerJButton.setEnabled( false );
			projectorVolumeJSlider.setEnabled( true );
			/*
			 * Adding in some sleep to prevent the frontend from changing back to the power on state.
			 * This might provide some time to get the correct status.  This should only
			 * sleep this piece from executing.... 
			 */
			try {
				Thread.sleep(500);
				System.out.println("Waiting for power off state to settle. Sleep 500ms.");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 2:  // on  Will BOLD the input that is currently selected.
			projectorStatusJTextArea.setText( "" );
			if( projectorControlSocket.status.getInput() == requestedInput)
			{
				System.out.println("Clearing the requested input variable!");
				requestedInput = -1;  //reset requested input 
			}
			for( int i = 0; i < inputJButton.length; i++ )  
			{
				inputJButton[i].setEnabled( true );
				if( projectorControlSocket.status.getInput() == i )  //active source
				{
					inputJButton[i].setFont( new Font( "SansSerif", Font.BOLD, 14 ) );
					//inputJButton[i].setBackground( new Color( 138, 240, 219));  //active input should be GREEN (111,255,120) = Power on Green!
					inputJButton[i].setBackground( new Color( 111, 255, 120)); // (Power on green! )
				}
				else if(requestedInput == i)
				{
					inputJButton[i].setFont( new Font( "SansSerif", Font.BOLD, 14 ) );
				}
				else  //inactive source
				{
					inputJButton[i].setFont( new Font( "SansSerif", Font.PLAIN, 14 ) );
					inputJButton[i].setBackground( new Color( 138, 205, 209 ) );  //non active input should be light blue
				}
			}
			projectorStandbyJButton.setEnabled( true );
			if( projectorControlSocket.status.muted() )
			{
				projectorStandbyJButton.setText( "Return from Standby" );
				projectorStandbyJButton.setBackground( new Color( 111, 255, 120 ) );
				projectorVolumeJSlider.setEnabled( false );
			}
			else
			{
				projectorStandbyJButton.setText( "Set Projector to Standby" );
				projectorStandbyJButton.setBackground( new Color( 255, 255, 68 ) );
				projectorVolumeJSlider.setEnabled( true );
				
				// Disallow the changing of the volume slider unless
				// the new value matches the value expected in the lock,
				// or the lock is disabled (value of -1)
				int newVolume = projectorControlSocket.status.getVolume();
				if ( volumeLock == -1 )
				{
					// No lock exists so we are at the server's mercy
					projectorVolumeJSlider.setValue( newVolume );
				}
				else if ( newVolume == volumeLock )
				{
					// The expected volume has met the unlock condition
					volumeLock = -1;
				}
				else if ( FORCE_VOLUME_ACQUIRE )
				{
					// We are still waiting for the returned status to meet
					// the unlock condition. Re-send the volume command to
					// break any possible deadlocks.
					projectorControlSocket.sendCommand( "volume " + 
							String.valueOf( volumeLock ));
				}
			}
			projectorPowerJButton.setEnabled( true );
			projectorPowerJButton.setText( "Turn Projector OFF" );
			projectorPowerJButton.setBackground( new Color( 255, 111, 120 ) );
			break;
		case 4:		// This state is returned for projectors that have communications on in standby mode.
			projectorStatusJTextArea.setText( "" );
			for( int i = 0; i < inputJButton.length; i++ )   //Clear any input styling that may be held or set. 
			{
				inputJButton[i].setEnabled( true );
				inputJButton[i].setFont( new Font( "SansSerif", Font.PLAIN, 14 ) );
				inputJButton[i].setBackground( new Color( 138, 205, 209 ) );  //non active input should be light blue
			}
			projectorStandbyJButton.setEnabled( false );
			projectorPowerJButton.setEnabled( true );
			projectorPowerJButton.setText( "Turn Projector ON" );
			projectorPowerJButton.setBackground( new Color( 111, 255, 120 ) );
			projectorVolumeJSlider.setEnabled( false );
			break;
		case 5:  //Error state
			projectorStatusJTextArea.setText("ERROR:  Call ITMS!");
			for( int i = 0; i < inputJButton.length; i++ )
			{
				inputJButton[i].setEnabled( false );
			}
			projectorStandbyJButton.setEnabled( false );
			projectorPowerJButton.setEnabled( false );
			projectorPowerJButton.setText( "Turn Projector ON" );
			projectorPowerJButton.setBackground( new Color( 111, 255, 120 ) );
			projectorVolumeJSlider.setEnabled( false );
			break;
		case 9: //This is for when the frontend is being used as a dummy...
			projectorPowerJButton.setEnabled(true);
			projectorPowerJButton.setText("Clone Display with Projector");
			projectorStandbyJButton.setVisible(false);
			projectorVolumeJSlider.setVisible(false);
			projectorStatusJTextArea.setVisible(false);
			projectorVolumeJLabel.setVisible(false);
			//now hide any input buttons that may be shown. 
			for ( int i = 0; i < inputJButton.length; i++ )
			{
				inputJButton[i].setVisible(false);
			}
			break;
		default: // off or unknown   This actually seems to be the default, for each startup.
			projectorStatusJTextArea.setText( "" );
			for( int i = 0; i < inputJButton.length; i++ )
			{
				inputJButton[i].setEnabled( true );
				inputJButton[i].setFont( new Font( "SansSerif", Font.PLAIN, 14 ) );
				inputJButton[i].setBackground( new Color( 138, 205, 209 ) );  //non active input should be light blue
			}
			projectorStandbyJButton.setEnabled( false );
			projectorPowerJButton.setEnabled( true );
			projectorPowerJButton.setText( "Turn Projector ON" );  //This is needed to revert to Turn Projector ON after turning the projector off.
			projectorPowerJButton.setBackground( new Color( 111, 255, 120 ) );
			projectorVolumeJSlider.setEnabled( false );
			while(1 < inputJButton.length)
			break;
		}
			
		//TODO remove this cruft.  Old Status checker. The issue with doing this here is that the frontend only checks for status every 6 seconds?  I need a way to push changes.
		/*if ( projectorPowerJButton.getText() == "Turn Projector ON")
		{	
			//projectorControlSocket.sendCommand( "status" );
		   Log.logEvent( " Checking status using default state" );
			}
		*/  	
		
	}// end changeState() method
	
     // main method
     public static void main( String[] args )
     {
    	
    	 /*
    	 * Two examples of look and feel setting try blocks.  The first is the suggested way to attempt to set a look and feel.  
    	 * This will itterate through all the installed lookandfeels and select one
    	 * Nimbus is the one set here.  It looks pretty bad on WIndows7/8 using the default jbuttons we use. 
    	 * 
    	 * We will stick with the default "Metal" Look and feel for this release.  While not pretty, is not 100% ugly.
    	 */
    	 
    	/* try {
    		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
    		        if ("Nimbus".equals(info.getName())) {
    		            UIManager.setLookAndFeel(info.getClassName());
    		            break;
    		        }
    		    }
    		} catch (Exception e) {
    		    // If Nimbus is not available, you can set the GUI to another look and feel.
    		}
    	 */
    	 try
    	{
    		//Set System L&F  This will match whichever OS it is currently running on.
	        UIManager.setLookAndFeel(
	        //    UIManager.getSystemLookAndFeelClassName());	//will use OS native look and feel.  Buttons don't look good on windows using this.
	        UIManager.getCrossPlatformLookAndFeelClassName()); //This will use the java look and feel.
	        }
    		catch (UnsupportedLookAndFeelException e) {
    		       // handle exception
    		    }
    		    catch (ClassNotFoundException e) {
    		       // handle exception
    		    }
    		    catch (InstantiationException e) {
    		       // handle exception
    		    }
    		    catch (IllegalAccessException e) {
    		       // handle exception
    		    }
    	 
    	 
    	 try
    	 {
    	
    		 new ProjectorControlFrontend( args[0], args[1], args[2] );
    		 
    	  
    	 }
    	 catch( ArrayIndexOutOfBoundsException e )
    	 {
    	//TODO
    	//	 check for the VDI variable.  Attempt to read it.  If populated with data try and connect to the VDI address.	 
    	//   Use another try catch?
    		 
	  
	    	String remote = WindowsRegistry.readRegistry("HKCU\\Volatile Environment", "ViewClient_IP_Address");
	    	System.out.println("VDI registry entry is: " + remote);
	        if(remote == null){
	        		//new ProjectorControlFrontend( "10.30.112.106:4444", "foo", "bar");//debug
	        		new ProjectorControlFrontend( "127.0.0.1:4443", "", "");//connect to local backend
	        }
	        else{
	           	//System.out.println(remote);
	        	new ProjectorControlFrontend( remote.trim() +":4444", "foo", "bar"); //connect to remote backend
	        	
	        }
    	 }
    	 
     } // end method main
     
     private void cleanShutdown()  //lets add something here to check if shutdown should actually turn off projector.  
     {
    	System.out.println("Clean Shutdown sequence");
    	 projectorControlSocket.sendCommand( "poweroff" );
    	 projectorControlSocket.close();
    	 System.out.println("Shutdown Complete");
    	 
    	 
    	 
     }
     
   private void projectorPowerJButtonActionPerformed( ActionEvent event )
   {
	   int modifiers = event.getModifiers();
	   if( modifiers == 19 )	// CTRL + SHIFT
	   {
		   System.exit( 0 );  //Runs the shutdown hook. Exit normally.
	   }
	   else
	   {
		   if( projectorPowerJButton.getText().equals( "Turn Projector ON" ) )
		   {
			   projectorControlSocket.sendCommand( "poweron" );
			   changeState(1);  //switch to the power on/off state manually.
			   requestedInput = projectorControlSocket.status.getRequestedinput();  //read the requested input from the backend.  This is a one time deal for reading the default input.
			  System.out.println("The default input is: " + requestedInput);
			
			   System.out.println("Projector powered on using interface");
			   Log.logEvent( "Projector powered on using interface" );
			   
			  displayClone();
			  
			  Log.logEvent( "Displays cloned by turning on projector");
//End Desktop Cloning.  This should probably be broken off into a class, and called when necessary.			   			   


		   }
		   else if(projectorPowerJButton.getText().equals( "Clone Display with Projector"))
		   {
			   displayClone();
		   }
		   else
		   {
			   projectorControlSocket.sendCommand( "poweroff" );
			   changeState(3);
			   Log.logEvent( "Projector powered off using interface" );
		   }
	   }
   }

   private void projectorStandbyJButtonActionPerformed( ActionEvent event )
   {
	   if( projectorStandbyJButton.getText().equals( "Set Projector to Standby" ) )
	   {
		   projectorControlSocket.sendCommand( "muteon" );
		   Log.logEvent( "Projector set to standby using interface" );
	   }
	   else
	   {
		   projectorControlSocket.sendCommand( "muteoff" );
		   Log.logEvent( "Projector returned from standby using interface" );
	   }
   }

   private void projectorHelpButton(ActionEvent event)
   {
	   //open a window displaying some help information.
	   Container contentPane = getContentPane();
	   JOptionPane.showMessageDialog(contentPane, "CCRI Projector Control Client " +
			   "v" + VERSION + "\n\n Call IT - Help Desk: 401.825.1112 \n\n "
			   		+ "Call IT - Media Services \n Lincoln:      401.333.7081 \n Knight:        401.825.2231 \n Liston:        401.455.6120\n"
			   		+ " Newport:    401.851.1708"
			   		+ "\n "
			   		+ "\nTroubleshooting:"
			   		+ "\nTry restarting the computer to resolve any issues\n"
			   		+ "\nUsage:"
			   		+ "\nClassroom PC will clone the projector and the desktop screen."
			   		+ "\nWill clone the classroom PC by default, or you can power on by choosing an input."
			   		+ "\nUsing a laptop requires the switch on the desk to be set to Laptop"
			   		+ "\nLogging off or shutting down will turn off the projector", "Help Message", JOptionPane.INFORMATION_MESSAGE);
   }
   private void inputJButtonActionPerformed( ActionEvent event )
   {
	   
	   //TODO:  Have the button that was pressed go bold immediately.  
	   InputJButton ijb = (InputJButton)event.getSource(); //return the button that originated the event.
	   int inputID = ijb.getInputID();
	   requestedInput = inputID;
	   //TODO: Set the input as the active input immediately!!  This will self correct when the status updates.	  
	   ijb.setFont( new Font( "SansSerif", Font.BOLD, 14 ) );
	  
		if( projectorControlSocket.status.getInput() == requestedInput)
		{
			System.out.println("Clearing the requested input variable!");
			requestedInput = -1;  //reset requested input 
		}
	   
	   if(projectorPowerJButton.getText().equals("Turn Projector ON") )  //Power the projector on before setting the input.
	   {
		   changeState(1);   // force the frontend to think its turning on!  The backend handles the power on stuff.
		   //projectorControlSocket.sendCommand("poweron");
	   }
	   
	   projectorControlSocket.sendCommand( "input " + String.valueOf( inputID ) );
	   
	   if(ijb.getText().contains("Classroom"))
	   {
		   displayClone();
		   
		   Log.logEvent( "Displays cloned by changing input.");
	   }
	   else
	   {} 
	   Log.logEvent( "Input #" + inputID + " selected using interface" );
   }
   
   public static void displayClone()
   {
	   try
	   {
		   
		   Runtime rt = Runtime.getRuntime() ;
		   p = rt.exec("c:\\windows\\system32\\displayswitch.exe /clone");
	 
	 //Next section not needed.
	   /* 
	    InputStream in = p.getInputStream() ;
	   OutputStream out = p.getOutputStream ();
	   InputStream err = p,getErrorStream() ;
	   do whatever you want
	   some more code
	   p.destroy() ;   is this needed?  probably to kill the process.  Display Swtich kills itself after a few seconds...
	   */
	   }catch(IOException exc){System.out.println("Displayswitch.exe not found!!");}   
	 
	   System.out.println("Displays Cloned");  
	     
	}
   public void minimizeWindow()
   {
	   setState( Frame.ICONIFIED );
	   Log.logEvent( "Application minimized by clicking minimize" );
   }
   
   public void checkScreenExtents()
   {
	   if(getLocation().x < 0) 
		{
		   centerUI();
		}
		if(getLocation().y < 0)
		{
			centerUI();
		}
		if(getLocation().x > screenWidth() - getSize().width )
		{
			centerUI();
		}
		if(getLocation().y > screenHeight() - getSize().height)
		{	
			centerUI();
		}
   }
   
   public int screenWidth()
   {
	   return Toolkit.getDefaultToolkit().getScreenSize().width;
   }
   
   public int screenHeight()
   {
	   return Toolkit.getDefaultToolkit().getScreenSize().height;
   }
   
   
  
public void centerUI()
   {   
	   
	   //Get current UI size
	   int uiWidth = getSize().width;
	   int uiHeight = getSize().height;  
	   
	 //Place the UI in the top center of the screen.
	   setLocation(screenWidth() /2 - uiWidth/2 , 0); 
   }
					   
	public void socketReconnect()
					    
	   {
		/*
		 * Automatically reconnect when the backend disappears and then returns.
		 * 
		 */
		changeState(5);
		projectorStatusJTextArea.setText("Reconnecting. Call Help \n Desk or RESTART \n Computer if message \ncontinues.");
	    	int i = 0;
			while(i==0) 
	    	{	
	       projectorControlSocket = null;
		   volumeLock = -1;
		   
			 
			   boolean connected = setupControlSocket( storedAddressPort, storedUser, storedPass );
			  
			   try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block  
				e.printStackTrace();
			}
			   
			   if ( connected )
			   {
				  
				   i=1; //we are reconnected
			   }
			  
			   
	    	}
	   }
   //End reconnect.
} // end class ProjectorControlFrontend

class ProjectorControlSocket implements Runnable
{
	public ConfigurationInfo configurationInfo = null;
	public Status status = null;
	private boolean continueConnection = true;
	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;
	private String user;
	private String pass;
	private StatusEventListener sEventListener;
	private ProjectorControlFrontend projectorControlFrontend;
	
	public ProjectorControlSocket( String address, String _user, String _pass, 
			ProjectorControlFrontend _projectorControlFrontend)
	{
		projectorControlFrontend = _projectorControlFrontend;
		user = _user;
		pass = _pass;
		
		boolean tryConnect = true;	// Try the connection or not
		int tries = 0;				// Number of tries to connect done so far
		int maxTries = 4;			// Maximum number of tries to connect
		
		while( tryConnect && tries < maxTries )
		{
			try
			{
				System.out.println( "Address should be: " + address );
				socket = new Socket( address.substring( 0, address.indexOf( ":" ) ) , 
						Integer.parseInt( address.substring( address.indexOf( ":" ) + 1 ) ) );
				try
				{
					out = new PrintWriter( socket.getOutputStream(), true );
					in = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );
					
					// Successful connection, no need to retry
					tryConnect = false;
				}
				catch( IOException e )
				{
					Log.logEvent( e.getMessage() );
					tries++;
				}
			}
			catch( UnknownHostException e )
			{
				Log.logEvent( e.getMessage() );
				tries++;
			}
			catch( IOException e )
			{
				Log.logEvent( e.getMessage() );
				tries++;
			}
			
			if( tryConnect && tries < maxTries )
			{
				// Wait before trying again
				Log.logEvent( "Server connect failure, waiting 5 seconds before retry." );
				try
				{
					Thread.sleep( 5000 );
				}
				catch( InterruptedException e ){}
			}
			else if ( tryConnect )
			{
				// Still trying to connect but no tries left
				continueConnection = false;
			}
		}
		if( continueConnection )
		{
			Thread t = new Thread( this );
			t.start();
		}
	}
	public void run()  //handles reading the responses.
	{
		boolean holdStatusReq = false;		// Hold off on status request flooding
		//boolean readFailed = false;
		while( continueConnection )
		{
			//here we should detect if the window has been moved.  
			//TODO:  offscreen check.
			
			//Need to get the screen extents in order to see if we have moved outside the bounds on the right side...
			
			//convert this to a method of it's own...
			
			projectorControlFrontend.checkScreenExtents();

			try
			{
				
				
				String response;  //read the input from the terminal.  Store last response...
					
				response = getResponse();   //gather the response from the backend.
					
					if( response.startsWith( "PROJECTOR>:")  )  //projectorBackend responds, or doesn't fail to respond after a status attempt.
					{
						if( configurationInfo == null )  //get configuration
							{
								sendCommand( "info" );
							}
						else if( status == null && !holdStatusReq )  //get initial status, after configuration info.
							{
								sendCommand( "status" );
								System.out.println("Currently: " + status);
								System.out.println("Getting initial status");
							}
						
						else if ( status.getAge() > 2000 && !holdStatusReq )   
							{
								// Only need to send one status request every 2 seconds  
								sendCommand( "status" );
								holdStatusReq = true;
								// System.out.println("Getting Status, too old " + status.getAge());
								try
								{
									//System.out.println("else, wait");
									Thread.sleep( 100 );  //wait a bit so we don't flood the telnet prompt.  THis seems to be a good value.
								}
								catch( InterruptedException e ){}
								//no need to wait here.... it doesn't process the response until.
							}
							
					}//back to the main IF
					else if( response.startsWith( "<?xml" ) )  //runs off previous status check.  
						{
							// Process the XML response and release the status hold
							System.out.println("Received XML");
							processXMLData( response );
							holdStatusReq = false;    
						}
					else if( response.equals( "login:" ) )
						{
							sendCommand( user );
						}
					else if( response.equals( "password:" ) )
						{
							sendCommand( pass );
						}
				
			}  //close the try
			catch( IOException e )  //catch the IOException from the getResponse() method.
			{
				Log.logEvent( e.getMessage() );
				continueConnection = false;
			}
			try
			{
				Thread.sleep( 50 );
			}
			catch( InterruptedException e ){}
			
		} // end of communication loop
		projectorControlFrontend.socketReconnect();	// Reconnect to the backend. Will display a message while trying to reconnect. Loops forever!
	} // end of run() method
	
	
	/*
	 * Causes the running connection loop to end
	 */
	public void shutdownConnection()
	{
		continueConnection = false;
	}
	
	/*
	 * Method to return projector response only when the projector has something usefull to say.  Otherwise we just 
	 * return a fake line.  If after so many fake lines returned we will respond will something that can be handled to discontinue 
	 * the connection.
	 * 
	 */
	public int fakeResponses = 0; //store the number of fake responses we have sent.  There should be a way to eliminate this entirely??
	
	public String getResponse() throws IOException
	{
		
		String response = "";
					
			if(in.ready()) //return true if the bufferedReader has anything to say :)
			{
				response = in.readLine().trim();  //read up to a linefeed/carriageReturn
				fakeResponses = 0;
			}
			
			else if(fakeResponses > 5)
			{
				response="MISSING PROJECTOR BACKEND";
				continueConnection = false;
			}
			
			else
			{
				response = "PROJECTOR>: FAKE RESPONSE";
				fakeResponses++;
				try
				{
					Thread.sleep(1000);
				}catch(InterruptedException e){}
			} 	
			
			System.out.println("Frontend Response: " + response);
		 return response;
	}
	
	private void processXMLData( String data )
	{
		try
		{
	        // System.out.println("Processing XML");
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
	        Document doc = docBuilder.parse( new InputSource( new StringReader( data ) ) );
            doc.getDocumentElement().normalize();
            if( doc.getDocumentElement().getNodeName().equals( "Configuration_Info" ) )
            {
	            System.out.println("Received Configuration Info" + System.currentTimeMillis());
            	configurationInfo =  new ConfigurationInfo( doc.getDocumentElement() );
            } else if( doc.getDocumentElement().getNodeName().equals( "Status" ) )
            {
            	if( status != null )
            	{
            		if ( status.newData( doc.getDocumentElement() ) )
            		{
            			fireStatusEvent();
            		}
            	}
            	else
            	{
            		status =  new Status( doc.getDocumentElement() );
            	}
            } 
        }
		catch (SAXParseException err){}
        catch (SAXException e)
        {
        	Exception x = e.getException ();
        	((x == null) ? e : x).printStackTrace ();
        }
        catch (Throwable t)
        {
        	t.printStackTrace ();
        }
        System.gc();
	}
	
	public void sendCommand( String command )
	{
		synchronized( this )
		{
			try
			{
				out.println( command );
				
				if ( command.length() != 0 )
					Log.logEvent( "Sent command to server: " + command );
				System.out.println( "Sent command to server: " + command );
					
			}
			catch( NullPointerException e )
			{
				Log.logEvent( "Failed to send command to server: " + command );
				System.out.println( "Failed to send command to server: " + command );
			}
		}
	}
	private void fireStatusEvent()
	{
		synchronized( this )
		{
			projectorControlFrontend.statusEventReceived( new StatusEvent( this ) );
		}
		Log.logEvent( "New status received from server: " + status.getState() );
		System.out.println("New status received from server:" + status.getState() + " age is:" + status.getAge());
	}
	public void close()
	{
		try
		{
			in.close();
		}
		catch( IOException e ){}
		catch( NullPointerException e ){}
		try
		{
			out.close();
		}
		catch( NullPointerException e ){}
		try
		{
			socket.close();
		}
		catch( IOException e ){}
		catch( NullPointerException e ){}
	}
} // end class ProjectorControlSocket
class ConfigurationInfo
{
	private String classroom;
	private String projectorType;
	private int projectorTypeID;
	private String connectionType;
	private int connectionTypeID;
	private String port;
	private String muteScreen;
	private String[] input;
	
	public ConfigurationInfo( Element xmlData )
	{
		classroom = xmlData.getElementsByTagName( "Classroom" )
			.item(0).getTextContent();
		projectorType = xmlData.getElementsByTagName( "Projector_Type" )
			.item(0).getTextContent();
		projectorTypeID = Integer.parseInt( xmlData.getElementsByTagName( 
			"Projector_Type" ).item(0).getAttributes().getNamedItem( 
			"id" ).getNodeValue() );
		connectionType = xmlData.getElementsByTagName( "Connection_Type" )
			.item(0).getTextContent();
		connectionTypeID = Integer.parseInt( xmlData.getElementsByTagName( 
			"Connection_Type" ).item(0).getAttributes().getNamedItem( 
			"id" ).getNodeValue() );
		port = xmlData.getElementsByTagName( "Port" )
			.item(0).getTextContent();
		muteScreen = xmlData.getElementsByTagName( "Mute_Screen" )
			.item(0).getTextContent();
		Node inputs = ( xmlData.getElementsByTagName( "Inputs" ).item(0) );
		NodeList inputList = ( (Element)inputs ).getElementsByTagName( "Input" );
		int numberOfInputs = inputList.getLength();
		input = new String[numberOfInputs];
		for( int i = 0; i < numberOfInputs; i++ )
		{
			input[i] = inputList.item(i).getTextContent();
		}
	}
	public String getClassroom()
	{
		return classroom;
	}
	public String getProjectorType()
	{
		return projectorType;
	}
	public String getConnectionType()
	{
		return connectionType;
	}
	public String getPort()
	{
		return port;
	}
	public String getMuteScreen()
	{
		return muteScreen;
	}
	public int getInputCount()
	{
		return input.length;
	}
	public String getInputName( int inputID )
	{
		return input[inputID];
	}
	public boolean hasSound()
	{
		return true;
	}
}
class Status
{
	private int state;
	private int input;
	private int source;
	private int mutescreen;
	private boolean mute;
	private boolean hreverse;
	private boolean vreverse;
	private int volume;
	private long lastUpdate;
	private int requestedInput;
	
	public Status( Element xmlData )
	{
		newData( xmlData );
	}
	public boolean newData( Element xmlData )
	{
		state = Integer.parseInt( xmlData.getElementsByTagName( "State" )
				.item(0).getTextContent() );
		input = Integer.parseInt( xmlData.getElementsByTagName( "Input" )
			.item(0).getTextContent() );
		requestedInput = Integer.parseInt( xmlData.getElementsByTagName( "Requested_Input" )
				.item(0).getTextContent() );
		try
		{
			source = Integer.parseInt( xmlData.getElementsByTagName( "SOURCE" )
					.item(0).getTextContent() );
		}
		catch( NumberFormatException e )
		{
			source = 0;
		}
		try
		{
			mutescreen = Integer.parseInt( xmlData.getElementsByTagName( "MSEL" )
					.item(0).getTextContent() );
		}
		catch( NumberFormatException e )
		{
			mutescreen = 0;
		}
		try
		{
			mutescreen = Integer.parseInt( xmlData.getElementsByTagName( "MSEL" )
					.item(0).getTextContent() );
		}
		catch( NumberFormatException e )
		{
			mutescreen = 0;
		}
		if( xmlData.getElementsByTagName( "MUTE" ).item(0).getTextContent().equals( "ON" ) )
		{
			mute = true;
		}
		else
		{
			mute = false;
		}
		if( xmlData.getElementsByTagName( "HREVERSE" ).item(0).getTextContent().equals( "ON" ) )
		{
			hreverse = true;
		}
		else
		{
			hreverse = false;
		}
		if( xmlData.getElementsByTagName( "VREVERSE" ).item(0).getTextContent().equals( "ON" ) )
		{
			vreverse = true;
		}
		else
		{
			vreverse = false;
		}
		try
		{
			volume = Integer.parseInt( xmlData.getElementsByTagName( "VOL" )
					.item(0).getTextContent() );
		}
		catch( NumberFormatException e )
		{
			volume = 0;
		}
		lastUpdate = System.currentTimeMillis();
		
		return true; // returns true if values have been changed
	}
	public long getAge()
	{
		//System.out.println("Last Status: " + (System.currentTimeMillis() - lastUpdate));
		return (System.currentTimeMillis() - lastUpdate);
	}
	public int getState()
	{
		return state;
	}
	public int getInput()
	{
		return input;
	}
	public int getSource()
	{
		return source;
	}
	public int getRequestedinput()
	{
		return requestedInput;
	}
	public int getMutescreen()
	{
		return mutescreen;
	}
	public boolean muted()
	{
		return mute;
	}
	public boolean hreversed()
	{
		return hreverse;
	}
	public boolean vreversed()
	{
		return vreverse;
	}
	public int getVolume()
	{
		return volume;
	}
}
class FadePanel extends JPanel    //This is what draws the background gradient :)
{
	private static final long serialVersionUID = 1L;
	public FadePanel()
	{
		setOpaque( false );
	}
	public void paintComponent( Graphics g )
	{
	    Graphics2D g2d = (Graphics2D) g;
	    Dimension d = getSize();
	    GradientPaint gp = new GradientPaint( 0, 0, new Color( 180,230,180 ),
	    		d.width*2/3, d.height/2, new Color( 40,90,40 ) );
	    Paint p = g2d.getPaint();
	    g2d.setPaint( gp );
	    g2d.fillRect( 0, 0, d.width, d.height );
	    g2d.setPaint( p );
	    super.paintComponent( g );
	}
} // end class FadePanel

class InputJButton extends JButton
{
	private static final long serialVersionUID = 1L;
	private int inputID;

	public int getInputID()
	{
		return inputID;
	}
	public void setInputID( int id )
	{
		inputID = id;
	}
}
/*
 * This class overrides the window closing and minimizing events.  Prevents the application from being hidden.
 * 
 */
class WindowHandler extends WindowAdapter
{
	public void windowClosing( WindowEvent event )
	{
	}
	public void windowIconified( WindowEvent event )
	{
	}
	public void windowsDeiconified (WindowEvent event)
	{
		
	}
}
interface StatusEventListener
{
	// listener interface. Interfaces must contain a blank method.  Not curly braces.
	public void statusEventReceived( StatusEvent event );
}

class StatusEvent extends EventObject
{
	private static final long serialVersionUID = 1L;

    public StatusEvent( Object source )
    {
        super( source );
    }
}

// Logger class responsible for event logging
class Log
{
	/**
	 * Logs events to an external CSV file
	 * @param eventText Message output to the log
	 */
	public static void logEvent( String eventText )
	{
		// Var to turn on/off logging
		final boolean LOGGING_ACTIVE = true;
		
		if (LOGGING_ACTIVE)
		{
			DecimalFormat twoDigitFormat = new DecimalFormat( "00" );
			Calendar logCalendar = Calendar.getInstance();
			String logDate = String.valueOf( logCalendar.get( Calendar.MONTH ) ) + 
			"/" + String.valueOf( logCalendar.get( Calendar.DAY_OF_MONTH ) ) +
			"/" + String.valueOf( logCalendar.get( Calendar.YEAR ) );
			String logTime = String.valueOf( logCalendar.get( Calendar.HOUR_OF_DAY ) ) + 
			":" + twoDigitFormat.format( logCalendar.get( Calendar.MINUTE ) ) +
			":" + twoDigitFormat.format( logCalendar.get( Calendar.SECOND ) );
			try
			{
				PrintWriter out = new PrintWriter(new FileWriter( "UsageLog.csv", true ) );
				out.println( logDate + "," + logTime + 
						"," + eventText + "," +
						System.getenv( "username" ) );
				out.close();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}
}
