@echo off
setlocal
REM use sc to stop, wait, then restart the projector control backend service
sc stop "Projector Control Backend"
REM use the following command to make the script wait 2 seconds for the backend stop. Will shut off projector if running. 
PING -n 5 127.0.0.1>nul
sc start "Projector Control Backend"
PING -n 5 127.0.0.1>nul
REM restart the backend and then exit after pausing for 5 seconds.